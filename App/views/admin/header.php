<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php
            if (isset($meta_title)) {
                echo $meta_title;
            } else {
                echo site_title();
            }
            ?></title>

        <!-- BEGIN META -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="<?php
        if (isset($meta_description)) {
            echo $meta_description;
        }
        ?>">
        <meta name="keywords" content="<?php
        if (isset($meta_keyword)) {
            echo $meta_keyword;
        }
        ?>">
        <!-- END META -->

        <!-- BEGIN STYLESHEETS -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/theme-default/bootstrap.css?1422792965" />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/theme-default/materialadmin.css?1425466319" />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/theme-default/font-awesome.min.css?1422529194" />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/theme-default/material-design-iconic-font.min.css?1421434286" />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/theme-default/libs/rickshaw/rickshaw.css?1422792967" />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/theme-default/libs/morris/morris.core.css?1420463396" />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/style.css" />
        <!-- END STYLESHEETS -->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script type="text/javascript" src="../../assets/js/libs/utils/html5shiv.js?1403934957"></script>
        <script type="text/javascript" src="../../assets/js/libs/utils/respond.min.js?1403934956"></script>
        <![endif]-->


        <!-- PAGE LEVEL STYLES -->
        <?php if (isset($includes_for_layout_css['css']) AND count($includes_for_layout_css['css']) > 0): ?>
            <?php foreach ($includes_for_layout_css['css'] as $css): ?>
                <link rel="stylesheet" type="text/css" href="<?php echo $css['file']; ?>"<?php echo ($css['options'] === NULL ? '' : ' media="' . $css['options'] . '"'); ?>>
            <?php endforeach; ?>
        <?php endif; ?>
        <!-- END PAGE LEVEL  STYLES -->


        <!-- PAGE LEVEL  Header Javascript -->
        <?php if (isset($includes_for_layout_js['js']) AND count($includes_for_layout_js['js']) > 0): ?>
            <?php foreach ($includes_for_layout_js['js'] as $js): ?>
                <?php if ($js['options'] !== NULL AND $js['options'] == 'header'): ?>
                    <script type="text/javascript" src="<?php echo $js['file']; ?>"></script>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>
        <!-- END PAGE LEVEL  Header Javascript -->


        <script>
            var base_url = "<?php echo base_url(); ?>";
        </script>
    </head>
    <body class="menubar-hoverable header-fixed ">

        <!-- BEGIN HEADER-->
        <header id="header" >
            <div class="headerbar">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="headerbar-left">
                    <ul class="header-nav header-nav-options">
                        <li class="header-nav-brand" >
                            <div class="brand-holder">

                                <span class="text-lg text-bold text-primary"><a class="navbar-brand" href="<?php echo base_url(); ?>"><?php if (site_logo() != '') { ?><img src="<?php echo base_url() . '/uploads/logo/' . site_logo(); ?>"><?php
                                        } else {
                                            echo site_title();
                                        }
                                        ?></a></span>

                            </div>
                        </li>
                        <?php
                        if ($this->session->userdata('logged_in')) {
                            ?>
                            <li>
                                <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                                    <i class="fa fa-bars"></i>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="headerbar-right">
                    <!--end .header-nav-options -->
                    <?php if ($currentuser) { ?>
                        <ul class="header-nav header-nav-profile">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
                                    <?php if ($currentuser->user_pic != '') { ?>
                                        <img class="img-circle size-2" src="<?php echo base_url(); ?>/uploads/users/profile/<?php echo $currentuser->user_pic; ?>" alt="" />
                                    <?php } else { ?>
                                        <img class="img-circle size-2" src="<?php echo base_url(); ?>/assets/img/default-user.jpg" alt="" />
                                    <?php } ?>
                                    <span class="profile-info">
                                        <?php echo $currentuser->first_name; ?>
                                        <small><?php echo $currentuser->role_name;?></small>
                                    </span>
                                </a>
                                <ul class="dropdown-menu animation-dock">
                                    <li class="dropdown-header">Profile Panel</li>
                                    <li><a href="<?php echo base_url('admin/users/details/' . $currentuser->id); ?>">My profile</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<?php echo base_url('admin/users/changepassword'); ?>"><i class="fa fa-fw fa-cog text-danger"></i> Change Password</a></li>
                                    <li><a href="<?php echo base_url('admin/users/logout'); ?>"><i class="fa fa-fw fa-power-off text-danger"></i> Logout</a></li>
                                </ul><!--end .dropdown-menu -->
                            </li><!--end .dropdown -->
                        </ul><!--end .header-nav-profile -->
                    <?php } ?>
                  <!--end .header-nav-toggle -->
                </div><!--end #header-navbar-collapse -->
            </div>
        </header>
        <!-- END HEADER-->