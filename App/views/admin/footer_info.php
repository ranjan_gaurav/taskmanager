<section>
    <div class="section-action style-primary">
        <div class="section-action-row">
            Developed By <a href="http://orangemantra.com/" class="ink-reaction">
                Orangemanta
            </a>
        </div>
        <div class="section-floating-action-row">
            <a class="btn ink-reaction btn-floating-action btn-lg btn-accent" href="<?php echo base_url('admin/home');?>" data-toggle="tooltip" data-placement="top" data-original-title="Dashboard">
                <i class="md md-home"></i>
            </a>
        </div>
    </div>
</section>