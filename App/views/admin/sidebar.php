<?php $id = $this->session->userdata('userid'); ?>
<div id="menubar" class="menubar-inverse ">
    <div class="menubar-fixed-panel">
        <div>
            <a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                <i class="fa fa-bars"></i>
            </a>
        </div>
        <div class="expanded">
            <a href="#">
                <span class="text-lg text-bold text-primary ">MATERIAL&nbsp;ADMIN</span>
            </a>
        </div>
    </div>
    <div class="menubar-scroll-panel">

        <!-- BEGIN MAIN MENU -->
        <ul id="main-menu" class="gui-controls">

            <!-- BEGIN DASHBOARD -->
            <li>
                <a href="<?php echo base_url('admin/home'); ?>" class="<?php echo getActiveMenu('home'); ?>">
                    <div class="gui-icon"><i class="md md-home"></i></div>
                    <span class="title">Dashboard </span>
                </a>
            </li><!--end /menu-li -->
            <!-- END DASHBOARD -->

            <!-- BEGIN EMAIL -->
            <?php if($id == 1){?>
            <li class="gui-folder">
                <a href="javascript:void(0);" class="<?php echo getActiveMenu('users'); ?>">
                    <div class="gui-icon"><i class="fa fa-users"></i></div>
                    <span class="title">Users</span>
                </a>
                <!--start submenu -->
                <ul>
                    <li><a href="<?php echo base_url('admin/users'); ?>" ><span class="title">User List</span></a></li>
                    <li><a href="<?php echo base_url('admin/users/create'); ?>" ><span class="title">Add New User</span></a></li>
                </ul><!--end /submenu -->
            </li><!--end /menu-li -->
            <!-- END EMAIL -->
          <?php }?>
       
            
            <li class="gui-folder">
                <a href="javascript:void(0);" class="<?php echo getActiveMenu('ManageInterviews'); ?>">
                    <div class="gui-icon"><i class="fa fa-glass"></i></div>
                    <span class="title">ManageCategories</span>
                </a>
                <!--start submenu -->
                <ul>
                    <li><a href="<?php echo base_url('admin/jobs/index'); ?>" ><span class="title">Category List</span></a></li>
                    <li><a href="<?php echo base_url('admin/jobs/create'); ?>" ><span class="title">Add New Category</span></a></li>
                     <li><a href="<?php echo base_url('admin/jobs/subcategory'); ?>" ><span class="title">SubCategory List</span></a></li>
                    <li><a href="<?php echo base_url('admin/jobs/createsubcategory'); ?>" ><span class="title">Add New SubCategory</span></a></li>
                   
                </ul><!--end /submenu -->
            </li><!--end /menu-li -->
            
            <?php if($id!=1){?>
              <li class="gui-folder">
                <a href="javascript:void(0);" class="<?php echo getActiveMenu('ManageInterviews'); ?>">
                    <div class="gui-icon"><i class="fa fa-mobile"></i></div>
                    <span class="title">ManageApplication</span>
                </a>
                <!--start submenu -->
                <ul>
                    <li><a href="<?php echo base_url('admin/users/details/' . $id); ?>" ><span class="title">Appliication List</span></a></li>
                   
                   
                </ul><!--end /submenu -->
            </li><!--end /menu-li -->
           
           <?php }?> 
           
             <li class="gui-folder">
                <a href="javascript:void(0);" class="<?php echo getActiveMenu('ManageInterviews'); ?>">
                    <div class="gui-icon"><i class="fa fa-glass"></i></div>
                    <span class="title">UsersQueries</span>
                </a>
                <!--start submenu -->
                <ul>
                    <li><a href="<?php echo base_url('admin/jobs/getUserQueries/'); ?>" ><span class="title">Queries List</span></a></li>
                  
                   
                </ul><!--end /submenu -->
            </li><!--end /menu-li -->
            
                     
            
            <!-- END LEVELS -->

        </ul><!--end .main-menu -->
        <!-- END MAIN MENU -->

        <div class="menubar-foot-panel">
            <small class="no-linebreak hidden-folded">
                <span class="opacity-75">Copyright &copy; 2016</span> <a href="http://orangemantra.com/" target="_blank"><strong>Orangemanta</strong></a>
            </small>
        </div>
    </div><!--end .menubar-scroll-panel-->
</div><!--end #menubar-->