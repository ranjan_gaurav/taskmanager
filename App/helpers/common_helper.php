<?php

/* This function is used to include the CSS and JS (Sanjay Yadav) */
if (!function_exists('add_includes')) {

    function add_includes($type, $files, $options = NULL, $prepend_base_url = TRUE) {
        $ci = &get_instance();
        $includes = array();
        if (is_array($files) and ! empty($files)) {
            foreach ($files as $file) {
                if ($prepend_base_url) {
                    $ci->load->helper('url');
                    $file = base_url() . $file;
                }

                $includes[$type][] = array(
                    'file' => $file,
                    'options' => $options
                );
            }
        } else {
            if ($prepend_base_url) {
                $ci->load->helper('url');
                $file = base_url() . $files;
            }

            $includes[$type][] = array(
                'file' => $file,
                'options' => $options
            );
        }

        return $includes;
    }

}

/* End Of function add_includes  */

if (!function_exists('getActiveMenu')) {

    function getActiveMenu($url) {
        $CI = &get_instance();
        $alias = $CI->uri->segment(2);
        $mfl = $CI->router->fetch_class();
        $mthd = $CI->router->fetch_method();
        if ($mfl == $url) {
            $class = "active";
        } else {
            $class = "";
        }
        return $class;
    }

}

if (!function_exists('check_auth')) {

    function check_auth() {
        $CI = &get_instance();
        $CI->load->module("users");
        //if (!$CI->users->_is_admin()) {
        if (!$CI->users->userdata()) {
            redirect('');
        }
    }

}

if (!function_exists('site_title')) {

    function site_title() {
        $CI = &get_instance();
        $CI->load->module("cms");
        $title = $CI->cms->getSitetitle();
        
        $title = @unserialize($title);
        return $title['c_title'];
    }

}
if (!function_exists('site_logo')) {

    function site_logo() {
        $CI = &get_instance();
        $CI->db->where('alias', 'logo');
        $query = $CI->db->get('cms');
        return $query->row()->content;
    }

}
if (!function_exists('state_name')) {

    function state_name($id) {
        $CI = &get_instance();
        $CI->db->where('id', $id);
        $query = $CI->db->get('state');
        if ($query->num_rows() > 0)
            return $query->row()->state_name;
    }

}
if (!function_exists('country_name')) {

    function country_name($id) {
        $CI = &get_instance();
        $CI->db->where('country_code', $id);
        $query = $CI->db->get('country');
        if ($query->num_rows() > 0)
            return $query->row()->country_name;
    }

}


if (!function_exists('getGoogleAnalyticCode')) {

    function getGoogleAnalyticCode($alias) {
        $CI = &get_instance();
        $CI->db->start_cache();
        $CI->db->where('alias', $alias);
        $query = $CI->db->get('cms');
        $CI->db->flush_cache();
        $qqq = $query->row();
        $yourCode = array();
        $yourCode['content'] = unserialize($qqq->content);
        return $yourCode['content']['description'];
    }

}

if (!function_exists('getPages')) {

    function getPages($slug = null) {
        $CI = &get_instance();
        $CI->db->start_cache();
        $con = array(
            'include_in' => $slug,
            'is_active' => '1',
            'is_deleted' => '0'
        );
        $CI->db->where($con);
        $query = $CI->db->get('pages');
        $CI->db->flush_cache();
        $qqq = $query->result();
        return $qqq;
    }

}

if (!function_exists('state_codes')) {

    function state_codes() {
        $CI = &get_instance();
        $query = $CI->db->get('state');
        $states = $query->result();
        $data = array();
        foreach ($states as $list) {
            $data[strtolower($list->state_name)] = $list->id;
        }
        return $data;
    }

}


if (!function_exists('site_limit')) {

    function site_limit() {
        $CI = &get_instance();
        $CI->load->module("cms");
        $title = $CI->cms->getSitetitle();
        $title = @unserialize($title);
        
       
        return $title['c_limit'];
    }

}

function admin_base_url($uri = '') {
    $CI = & get_instance();
    return $CI->config->base_url(ADMIN . '/' . $uri);
}

function admin_url($uri = '') {
    $CI = & get_instance();
    return $CI->config->site_url(ADMIN . '/' . $uri);
}

function admin_redirect($uri = '', $method = 'location', $http_response_code = 302) {
    if (!preg_match('#^https?://#i', $uri)) {
        $uri = admin_url($uri);
    }

    switch ($method) {
        case 'refresh' : header("Refresh:0;url=" . $uri);
            break;
        default : header("Location: " . $uri, TRUE, $http_response_code);
            break;
    }
    exit;
}

$seoURL = '';

function base_url_without_slash() {
    return substr(base_url(), 0, strlen(base_url()) - 1);
}

function getHref($menu_id) {
    global $seoURL;
    $seoURL = '';
    return gethyperlink($menu_id);
}

function gethyperlink($menu_id) {
    global $seoURL;
    $href = '';
    $hrefRw = '';

    $ci = & get_instance();
    $ci->load->database();

    if (is_int($menu_id) || $menu_id != '') {
        $query = $ci->db->query("SELECT * FROM menu WHERE menu_id = $menu_id AND menu_status = 'Active'");
        if ($query->num_rows() > 0) {
            $hrefRw = $query->row_array();
            if (strtolower($hrefRw['menu_name']) == 'home') {
                return $href = " href = '" . base_url() . "'";
            } else if ($hrefRw['menu_page_type'] == 'Page') {
                $seoURL = "/" . $hrefRw['menu_slug'] . $seoURL;
                gethyperlink($hrefRw['menu_parent_id']);
            } else {
                if (!strstr($hrefRw['menu_link'], "http://") && !strstr($hrefRw['menu_link'], "https://")) {
                    if (!strstr($hrefRw['menu_link'], "http://")) {
                        $href = " href = 'http://" . $hrefRw['menu_link'] . "'";
                    }
                } else {
                    $href = " href = '" . $hrefRw['menu_link'] . "'";
                }
                if ($hrefRw['menu_link_type'] == '_blank') {
                    $href .= " target = '_blank'";
                }
            }
        }
        if (isset($hrefRw['menu_page_type'])) {
            if ($hrefRw['menu_page_type'] == 'Page') {
                $href = " href = '" . base_url_without_slash() . $seoURL . ".html'";
            }
        }
        return $href;
    } else {
        return false;
    }
}

function set_pagination() {
    $CI = &get_instance();
    $CI->page_config['full_tag_open'] = '<div class="pagination pagination-small pagination-right"><ul class="pagination">';
    $CI->page_config['full_tag_close'] = '</ul></div>';
    $CI->page_config['first_link'] = true;
    $CI->page_config['last_link'] = true;
    $CI->page_config['first_tag_open'] = '<li>';
    $CI->page_config['first_tag_close'] = '</li>';
    $CI->page_config['prev_link'] = '&laquo';
    $CI->page_config['prev_tag_open'] = '<li class="prev">';
    $CI->page_config['prev_tag_close'] = '</li>';
    $CI->page_config['next_link'] = '&raquo';
    $CI->page_config['next_tag_open'] = '<li>';
    $CI->page_config['next_tag_close'] = '</li>';
    $CI->page_config['last_tag_open'] = '<li>';
    $CI->page_config['last_tag_close'] = '</li>';
    $CI->page_config['cur_tag_open'] = '<li class="active"><a href="#">';
    $CI->page_config['cur_tag_close'] = '</a></li>';
    $CI->page_config['num_tag_open'] = '<li>';
    $CI->page_config['num_tag_close'] = '</li>';
    return $CI->page_config;
}

function addhttp($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
}

/* Function to send the notification * /
 * 
 * 
 */
if (!function_exists('insertNotification')) {

    function insertNotification($data) {
        $CI = &get_instance();
        $CI->db->insert('notifications', $data);
    }

}

if (!function_exists('androidNotification')) {

    function androidNotification($deviceIDS, $data = '') {

        $CI = &get_instance();
        $getuser = $deviceIDS;
        foreach ($getuser as $val) {
            $register_send = $val;
            $value = $register_send['reg_id'];
            $get = json_decode($value);
            $message = $data['message'];
            $title = $data['title'];
            $notiType = $data['type'];

            $notificationUpdateData = array(
                'resource_id' => $data['resource_id'],
                'user_id' => $val['id'],
                'type' => $data['type'],
                'title' => $title,
                'message' => $message,
                'device_type' => 'android',
                'status' => '0'
            );

            insertNotification($notificationUpdateData);
            $api_key = 'AIzaSyAUuOPKADanekXAwpLUwiJuMbzQ5PVXX0A';

            if (empty($api_key) || count($get) < 0) {
                $result = array(
                    'success' => '0',
                    'message' => 'api or reg id not found',
                );
                echo json_encode($result);
                die;
            }
            $registrationIDs = $get;
            $url = 'https://android.googleapis.com/gcm/send';
            $fields = array(
                'registration_ids' => $registrationIDs,
                'data' => array("message" => $message, "title" => $title, "type" => $notiType, 'data' => $data['data']),
            );

            $headers = array(
                'Authorization: key=' . $api_key,
                'Content-Type: application/json');

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);

            curl_close($ch);
            //print_r($result);
            //die;
        }
    }

}

// ios push notificatin apple mac 
if (!function_exists('iosNotification')) {

    function iosNotification($deviceIDS, $data = '') {
        $url = IMAGESPATH . 'ios-certified/LiveCert.pem';
        $CI = &get_instance();
        $getuser = $deviceIDS;
        $sound = 'default';
        $badge = '0';
        $content_aval = '1';
        $message = $data['message'];
        $title = $data['title'];
        foreach ($getuser as $valios) {
            //print_r($valios); 
            $register_send = $valios;
            $value = $register_send['reg_id'];
            $gets = json_decode($value);

            $notificationUpdateData = array(
                'resource_id' => $data['resource_id'],
                'user_id' => $valios['id'],
                'type' => $data['type'],
                'title' => $title,
                'message' => $data['message'],
                'device_type' => 'ios',
                'status' => '0'
            );

            insertNotification($notificationUpdateData);
            foreach ($gets as $get) {
                $dtoken = $get;
                $message = $message;
                // My device token here (without spaces):
                $deviceToken = $dtoken;
                // print_r($deviceToken); die;
                // My private key's passphrase here:
                $passphrase = 'BCG4pp$';

                // My alert message here:
                //$message = 'New Push Notification!';
                //badge
                $badge = 0;

                $ctx = stream_context_create();
                stream_context_set_option($ctx, 'ssl', 'local_cert', $url);
                stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

                // Open a connection to the APNS server
                $fp = stream_socket_client(
                        'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

                if (!$fp)
                    exit("Failed to connect: $err $errstr" . PHP_EOL);

                //echo 'Connected to APNS' . PHP_EOL;
                // Create the payload body
                $body['aps'] = array(
                    'alert' => $message,
                    'badge' => $badge,
                    'sound' => 'default'
                );
                $body['data'] = $data;

                // Encode the payload as JSON
                $payload = json_encode($body);

                // Build the binary notification
                $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

                // Send it to the server
                $result = fwrite($fp, $msg, strlen($msg));

                /* if (!$result)
                  echo 'Error, notification not sent' . PHP_EOL;
                  else
                  echo 'notification sent!' . PHP_EOL; */

                // Close the connection to the server

                fclose($fp);
            }
            //print_r($result);
            //die;
        }
    }

}
/*
 * To get The Non Action Based Notification details based Upon the Alias of that Notification.
 */

if (!function_exists('GetNonActionNotificationDetails')) {

    function GetNonActionNotificationDetails($alias) {
        $CI = &get_instance();
        $CI->db->where('name', $alias);
        $CI->db->select('title,message');
        $CI->db->from('custom_notification');
        $res = $CI->db->get();
        return $res->row();
    }

}
?>