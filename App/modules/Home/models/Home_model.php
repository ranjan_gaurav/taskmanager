<?php

/*
 *
 */
class Home_model extends CI_Model {
	var $table = "application";
	var $user = "user";
	
	function __construct() {
		parent::__construct ();
		$this->load->library ( 'session' );
		$this->load->library ( 'pagination' );
	}
	
   public function UploadProfilePhoto() {
        $config = array();
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 6000);
        ini_set('max_execution_time', 6000);
        $config['upload_path'] = IMAGESPATH . 'users/profile/';
        $config['allowed_types'] = '*';
        $config['file_name'] = md5(uniqid(rand(), true));
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('user_pic')) {
            $info = $this->upload->data();
            return $info;
        }
    }
	
	public function create_app()
	{
	$file = $this->UploadProfilePhoto();
        if ($file['file_name'] != '') {
            $data['user_pic'] = $file['file_name'];
        }
		$data['app_title'] = $this->input->post('app_title', true);
		$data['app_name'] = $this->input->post('app_name', true);
		$data['userid'] = $this->input->post('userid',true);
	//	$app = $this->input->post();
	//	unset($app['submit']);
		$this->db->set('created_date', 'NOW()', false);
		$this->db->insert($this->table, $data);
		$data['app_id'] = $this->db->insert_id();
	
	
		$id = $this->session->userdata('userid');
		$val['app_id'] = $data['app_id'];
		$this->db->where ( 'id', $id );
		$update = $this->db->update ( 'users', $val );
		
		
	 //  echo $this->db->last_query();die();
		
		if ($update) {
			
			return true;
		}
		else {
			return false;
		}
		
	}
	
	public function update_visit_status($id)
	{
		
		$data['visit_status'] = "1";
		$this->db->where ( 'id', $id );
		$query = $this->db->update ( 'users', $data );
		
		//echo $this->db->last_query(); die();
		
		if ($query) {
			return true;
		} else {
			return false;
		}
	}
	
	

	public function get_visit_status($id)
	{
		$this->db->where ('id', $id );
	    $query = $this->db->get ( 'users' );
		if ($query->num_rows () > 0) {
			return $query->row ();
		} else {
			return false;
		}
	}
	
}

?>