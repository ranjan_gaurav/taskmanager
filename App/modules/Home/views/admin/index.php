
<section>
	<div class="section-body">
		<div class="row">
			<!-- BEGIN ALERT - REVENUE -->
			<div class="col-md-3 col-sm-6">
				<div class="card">
					<div class="card-body no-padding">
						<div class="alert alert-callout alert-info no-margin">
							<strong class="pull-right text-success text-lg">100 <i
								class="fa fa-users"></i></strong> <strong class="text-xl">Total
								Users</strong><br /> <span class="opacity-50">Total Users</span>
						</div>
					</div>
					<!--end .card-body -->
				</div>
				<!--end .card -->
			</div>
			<!--end .col -->
			<!-- END ALERT - REVENUE -->

			<!-- BEGIN ALERT - VISITS -->
			<div class="col-md-3 col-sm-6">
				<div class="card">
					<div class="card-body no-padding">
						<div class="alert alert-callout alert-warning no-margin">
							<strong class="pull-right text-warning text-lg">0,01% <i
								class="md md-swap-vert"></i></strong> <strong class="text-xl">432,901</strong><br />
							<span class="opacity-50">Visits</span>
							<div class="stick-bottom-right">
								<div class="height-1 sparkline-visits" data-bar-color="#e5e6e6"></div>
							</div>
						</div>
					</div>
					<!--end .card-body -->
				</div>
				<!--end .card -->
			</div>
			<!--end .col -->
			<!-- END ALERT - VISITS -->

			<!-- BEGIN ALERT - BOUNCE RATES -->
			<div class="col-md-3 col-sm-6">
				<div class="card">
					<div class="card-body no-padding">
						<div class="alert alert-callout alert-danger no-margin">
							<strong class="pull-right text-danger text-lg">0,18% <i
								class="md md-trending-down"></i></strong> <strong
								class="text-xl">42.90%</strong><br /> <span class="opacity-50">Bounce
								rate</span>
							<div class="stick-bottom-left-right">
								<div class="progress progress-hairline no-margin">
									<div class="progress-bar progress-bar-danger"
										style="width: 43%"></div>
								</div>
							</div>
						</div>
					</div>
					<!--end .card-body -->
				</div>
				<!--end .card -->
			</div>
			<!--end .col -->
			<!-- END ALERT - BOUNCE RATES -->

			<!-- BEGIN ALERT - TIME ON SITE -->
			<div class="col-md-3 col-sm-6">
				<div class="card">
					<div class="card-body no-padding">
						<div class="alert alert-callout alert-success no-margin">
							<h1 class="pull-right text-success">
								<i class="md md-timer"></i>
							</h1>
							<strong class="text-xl">54 sec.</strong><br /> <span
								class="opacity-50">Avg. time on site</span>
						</div>
					</div>
					<!--end .card-body -->
				</div>
				<!--end .card -->
			</div>
			<!--end .col -->
			<!-- END ALERT - TIME ON SITE -->

		</div>
		<!--end .row -->
		<div class="row">
		   <?php if ($this->session->flashdata('message')) { ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('message'); ?>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php } ?>

			<!-- BEGIN SITE ACTIVITY -->
			<?php  if($visit == 0){?>
			<div class="col-md-12">
				<div class="card ">
					<div class="row">
						<div class="col-md-12">
							<div class="card-actionbar">
								<div class="card-actionbar-row">
									<button type="submit" id="submit"
										class="btn ink-reaction btn-raised btn-primary btn-loading-state"
										onclick="openModal();" data-loading-text="<i  
										class='fa fa-spinner fa-spin'>
										</i> Loading...">Get Started
										<div style="top: 26px; left: 32.5px;" class="ink"></div>
									</button>
								</div>
							</div>
							<!--end .card-body -->
						</div>
						<!--end .col -->
					</div>
					<!--end .row -->
				</div>
				<!--end .card -->
			</div>
			<?php }?>
			<!--end .col -->
			<!-- END SITE ACTIVITY -->
		</div>
		<!--end .row -->
	</div>
	<!--end .section-body -->
</section>


<div id="myModal12" class="modal">
	<div id="modal-content" class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-hidden="true">&times;</button>
		</div>
		<div class="modal-body">
			
            <?php
												$this->load->view ( 'modalperformance' );
												?>
        </div>
	</div>
</div>


<script type="text/javascript">

function openModal(){
	
	 $("#myModal12").slideDown(1000);

    $('#myModal12').modal();
}       
</script>