<?php $id = $this->session->userdata('userid'); ?>
<script
	src="<?php echo base_url('assets/plugins/ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript"
	src="<?php echo base_url('assets/plugins/ckfinder/ckfinder.js'); ?>"></script>
	<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
<section style="padding: 0px;">
	<div class="contain-lg">
		<!-- BEGIN BASIC VALIDATION -->
		<div class="row">
			<div class="col-md-12">
				<div class="card-body form-inverse">
					<form class="form form-validate floating-label"
						novalidate="novalidate" method="post">
						<div class="card-head style-primary">
							<div class="tools pull-left">
								<header>Create Application</header>
							</div>
							
						</div>
						<div class="card">
						 <form class="form" role="form form-validate" method="post"  id="userForm" enctype="multipart/form-data">
							<div class="card-body">
                                <?php if (@$error): ?>
                                    <div
									class="alert alert-callout alert-warning" role="alert">
									<strong>Warning!</strong> <?php echo $error; ?>
                                    </div>
                                <?php endif; ?>
<!--                                 <div class="form-group"> -->
								<!--                                     <textarea name="title" id="textarea1" class="form-control" rows="3" required></textarea> -->
								<!--                                     <label for="title">Blog Title</label> -->
								<!--                                 </div> -->

								<!--                                 <div class="form-group"> -->
								<!--                                     <textarea id="editor1" class="form-control control-5-rows" placeholder="Enter text ..." name="description"> -->
								<!--                                     </textarea> -->

								
								
								
								
								        	<div class="form-group">
									<input type="text" class="form-control" id="title" name="app_title"
										> <label
										for="meta_description">Application Title</label>
								</div>
								
								
								<div class="form-group">
									<input type="text" class="form-control" id="name" name="app_name" required data-rule-minlength="2" 
										> <label
										for="meta_description">Application Name</label>
								</div>
								
								
                                  
								
                             <div class="col-md-4">
                                        <div class="form-group">
                                            
                                            <div class="form-group">
                                                <input class="form-control" type="file" name="app_logo" id="app_logo">
                                            </div>

                                        </div>
                                    </div>

                                 <input type="hidden" id="userid" name="userid" value='<?php echo $id;?>'/>



							</div>
							<!--end .card-body -->
							<div class="card-actionbar">
								<div class="card-actionbar-row">
									<button type="submit" id="submit" name="submit"
										class="btn ink-reaction btn-raised btn-primary btn-loading-state"
										data-loading-text="<i  class='fa fa-spinner fa-spin'>
										</i> Loading...">Create
										<div style="top: 26px; left: 32.5px;" class="ink"></div>
									</button>
								</div>
							</div>
							
							</form>
							<!--end .card-actionbar -->
						</div>
						<!--end .card -->
					</form>
				</div>
			</div>
			<!--end .col -->
		</div>
		<!--end .row -->
		<!-- END BASIC VALIDATION -->

	</div>
	<!--end .section-body -->
</section>

<script type="text/javascript">
function HandleBrowseClick()
{
    var fileinput = document.getElementById("app_logo");
    fileinput.click();
}

function Handlechange()
{
    var fileinput = document.getElementById("app_logo");
    var textinput = document.getElementById("filename");
    textinput.value = fileinput.value;
}
</script>


