<?php
class Home extends Admin_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->model ( 'Home_model' );
		$this->load->library ( 'form_validation' );
		$this->load->library ( 'session' );
		$this->load->helper('url');
	}
	function index() {
		
		
		if (! $this->session->userdata ( 'logged_in' )) {
			redirect ( base_url ( 'admin/users/signin' ) );
		}
		// $visit = $this->session->userdata('visit');
		$id = $this->session->userdata ( 'userid' );
		$includeJs = array(
            'assets/js/libs/jquery-validation/dist/jquery.validate.min.js',
            'assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js',
            'assets/js/core/demo/DemoFormComponents.js'
        );
       
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
		
		if (isset ( $_POST ['submit'] )) {
			
			$this->form_validation->set_rules ( 'app_name', 'appliaction_name', 'required' );
			if ($this->form_validation->run () == false) {
				
				
				//$data['error'] = validation_errors();
				//$data['main_content'] = 'index';
			} 

			else {
				$query = $this->Home_model->create_app ();
				
				if ($query) {
					
					$insert = $this->Home_model->update_visit_status ( $id );
					if ($insert) {
						$this->session->set_flashdata ( 'message', 'Application Created Successfully.' );
					} else {
						$this->session->set_flashdata ( 'error', 'Something Went Wrong' );
					}
				}
				redirect ( 'admin/Home' );
			}
		}
		
		$getStatus = $this->Home_model->get_visit_status ( $id );
		$visit = $getStatus->visit_status;
		$data ['visit'] = $visit;
		
		$data ['main_content'] = 'index';
		$this->setData ( $data );
	}
}

?>