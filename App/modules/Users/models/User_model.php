<?php

/*

 */

class User_model extends CI_Model {

    var $table = "users";
    var $qualification = "qualifications";
    var $usersFeilds = "user_fields";
    var $table_fields_values = "user_fields_values";
    var $app = "application";
    var $original_path;
    var $resized_path;
    var $thumbs_path;

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('pagination');
    }

    function read($showPerpage, $offset) {
        $this->db->limit($showPerpage, $offset);
        $this->db->from($this->table);
        $query = $this->db->get();
        return $query->result();
    }

    function usersFeilds($showPerpage, $offset) {
        $this->db->limit($showPerpage, $offset);
        $this->db->from($this->usersFeilds);
        $query = $this->db->get();
        return $query->result();
    }

    function count_all() {
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }

    function user_by_id($id) {
        $this->db->where('id', $id);
        $query = $this->db->get($this->table);
        $query->row()->role = $this->get_role($id);
        $query->row()->role_name = $this->get_role_name($query->row()->role);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function user_by_nicename($user_nicename) {
        $this->db->where('username', $user_nicename);
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0) {
            return $this->user_by_id($query->row()->id);
        } else {
            return false;
        }
    }

    function update($userid, $userdata) {
        $data = (array) $userdata;
        $file = $this->UploadProfilePhoto();
        if ($file['file_name'] != '') {
            $data['user_pic'] = $file['file_name'];
        }
        $where = "id = $userid";
        $str = $this->db->update_string($this->table, $data, $where);
        $query = $this->db->query($str);
        return $query;
    }
    
    public function deleteexistingProfileImage($file, $id) {
        unlink(IMAGESPATH . 'users/profile/' . $file);
        $data['user_pic'] = '';
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
    }

    function delete() {
        
    }

    function get_role($user_id) {
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('users_roles');
        if ($query->num_rows() > 0) {
            return (int) $query->row()->role_id;
        } else {
            return 0;
        }
    }

    function get_roles() {
    	$this->db->where_not_in('name','admin');
        $query = $this->db->get('roles');
        return $query->result();
    }

    function get_role_name($role_id) {
        $this->db->where('id', $role_id);
        $query = $this->db->get('roles');
        if ($query->num_rows() > 0) {
            return $query->row()->name;
        } else {
            return false;
        }
    }

    function validate($user_email, $password) {
        $this->db->where('email', $user_email);
        $this->db->where('password', $password);
        $query = $this->db->get($this->table);
     //   echo $this->db->last_query(); die();
        if ($query->num_rows() === 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function UploadProfilePhoto() {
        $config = array();
        ini_set('upload_max_filesize', '200M');
        ini_set('post_max_size', '200M');
        ini_set('max_input_time', 6000);
        ini_set('max_execution_time', 6000);
        $config['upload_path'] = IMAGESPATH . 'users/profile/';
        $config['allowed_types'] = '*';
        $config['file_name'] = md5(uniqid(rand(), true));
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('user_pic')) {
            $info = $this->upload->data();
            return $info;
        }
    }

    function create_user() {
        $file = $this->UploadProfilePhoto();
        if ($file['file_name'] != '') {
            $data['user_pic'] = $file['file_name'];
        }
        $data['first_name'] = $this->input->post('first_name', true);
        $data['last_name'] = $this->input->post('last_name', true);
        $data['password'] = md5($this->input->post('user_pass', true));
        $data['email'] = $this->input->post('user_email', true);
        $data['mobile'] = $this->input->post('user_mobile', true);
        $data['location'] = $this->input->post('user_city', true);
        $data['user_gender'] = $this->input->post('user_gender', true);
        $data['status'] = '0';
        $this->db->set('registered_date', 'NOW()', false);
        $query = $this->db->insert($this->table, $data);
        if ($query) {
            $user_id = $this->db->insert_id();
            $user_value = $this->input->post('role_id') ? $this->input->post('role_id') : '3';
            $this->update_role($user_id, $user_value);
            return true;
        } else {
            return false;
        }
    }

    function update_role($userID, $value) {
        $data = array(
            'user_id' => $userID,
            'role_id' => $value
        );
        $this->db->insert('users_roles', $data);
    }

    function updateRole($userID, $value) {
        $data = array(
            'role_id' => $value
        );
        $this->db->where('user_id', $userID);
        $this->db->update('users_roles', $data);
    }

    function updateStatus() {
        if ($this->input->post('Delete') != null) {
            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('id', $arr_ids);
                $this->db->delete($this->table);
            }
        } else {
            if ($this->input->post('Deactivate') != null) {
                $status = "0";
            } else if ($this->input->post('Activate') != null) {
                $status = "1";
            }
            $data = array(
                'status' => $status
            );

            $arr_ids = $this->input->post('arr_ids');
            if (!empty($arr_ids)) {
                $this->db->where_in('id', $arr_ids);
                $this->db->update($this->table, $data);
            }
        }
    }

    function reset_password($key) {
        $data = array(
            'user_pass' => md5($this->input->post('new_password')),
        );
        $this->db->where('activation_key', $key);
        $query = $this->db->update($this->table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    function getUserKey($email) {
        $this->db->where('user_email', $email);
        $query = $this->db->get($this->table);
        return $query->row()->activation_key;
    }

    function currentPassword($userID) {
        $this->db->where('id', $userID);
        $query = $this->db->get($this->table);
        return $query->row()->user_pass;
    }

    function change_password($user_id, $password) {
        $data = array(
            'user_pass' => md5($password),
        );
        $this->db->where('id', $user_id);
        $query = $this->db->update($this->table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }
    
    function app_details($nicename)
    {
    	$this->db->where('userid', $nicename);
    	$query = $this->db->get($this->app);
    	
    	//echo $this->db->last_query(); die();
    	
    		return $query->row();
    	 
    	
    }
    

}

?>