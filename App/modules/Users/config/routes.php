<?php

//$route['users'] = "Users/list_all";
$route['users/forgot_password'] = "Users/forgot_password";
$route['users/reset_password'] = "Users/reset_password";
$route['users/logout'] = "Users/logout";
$route['users/account'] = "Users/account";
$route['users/account/id/(:any)'] = "Users/account/$1";
$route['users/account/id/(:any)'] = "Users/account/$1";
$route['users/signin'] = "Users/signin";
$route['users/signup'] = "Users/signup";
$route['users/create'] = "users/create";
$route['users/mongo'] = "Users/mongo";
$route['users/profile/(:any)'] = "Users/user/$1";
?>