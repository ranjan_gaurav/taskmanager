<section>
    <div class="section-body contain-lg">
        <div class="row">

            <!-- BEGIN ADD CONTACTS FORM -->
            <div class="col-md-12">
                <div class="card">
                    <div class="card-head style-primary">
                        <div class="tools pull-left">
                            <header>Update User : </header>
                        </div>
                        <div class="tools">
                            <a class="btn btn-flat hidden-xs" href="<?php echo base_url('admin/users'); ?>"><span class="glyphicon glyphicon-arrow-left"></span> &nbsp;Back</a>
                        </div>
                    </div>
                    <form class="form" role="form" method="post" action="" id="userForm" enctype="multipart/form-data">

                        <!-- BEGIN DEFAULT FORM ITEMS -->
                        <?php if (@$error): ?>
                            <div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <?php echo $error; ?>
                            </div>
                        <?php endif; ?>
                        <?php if ($this->session->flashdata('message')) { ?>
                            <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success ! </strong> <?php echo $this->session->flashdata('message'); ?>
                            </div>
                        <?php } ?>
                        <div class="card-body style-primary form-inverse">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group floating-label">
                                                <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $user->first_name; ?>" />
                                                <label for="first_name">FirstName</label>
                                            </div>
                                        </div><!--end .col -->
                                        <div class="col-md-6">
                                            <div class="form-group floating-label">
                                                <input type="text" class="form-control" id="username" name="username" value="<?php echo $user->last_name; ?>" />
                                                <label for="username">LastName</label>
                                            </div>
                                        </div><!--end .col -->
                                    </div><!--end .row -->
                                </div><!--end .col -->
                            </div><!--end .row -->
                        </div><!--end .card-body -->
                        <!-- END DEFAULT FORM ITEMS -->

                        <!-- BEGIN FORM TABS -->
                        <div class="card-head style-primary">
                            <ul class="nav nav-tabs tabs-text-contrast tabs-accent" data-toggle="tabs">
                                <li class="active"><a href="#contact">CONTACT INFO</a></li>
<!--                                 <li><a href="#general">Notes</a></li> -->
                            </ul>
                        </div><!--end .card-head -->
                        <!-- END FORM TABS -->

                        <!-- BEGIN FORM TAB PANES -->
                        <div class="card-body tab-content">
                            <div class="tab-pane active" id="contact">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="user_mobile" name="user_mobile" value="<?php echo $user->mobile; ?>" />
                                                    <label for="user_mobile">Mobile</label>
                                                </div>
                                            </div><!--end .col -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="input-group date" id="demo-date">
                                                        <div class="input-group-content">
                                                            <input type="text" class="form-control" name="user_dob" value="<?php //echo date('m/d/Y', strtotime($user->user_dob)); ?>" required>
                                                            <label>Date of Birth</label>
                                                        </div>
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div><!--end .col -->

                                        </div><!--end .row -->
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="inputEmail" placeholder="Email" name="email" value="<?php echo $user->email; ?>" />
                                            <label for="user_email">Email</label>
                                        </div><!--end .form-group -->
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <input type="password" class="form-control" id="inputPassword" placeholder="Password" name="password" value="" />
                                                    <label for="inputPassword">Password</label>
                                                </div>
                                            </div><!--end .col -->
                                        </div><!--end .row -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <select class="form-control" name="user_gender" id="user_gender">
                                                        <option value="">Select</option>
                                                        <option value="Male" <?php
                                                        if ($user->user_gender == 'Male') {
                                                            echo "selected='selected'";
                                                        }
                                                        ?>>Male</option>
                                                        <option value="Female" <?php
                                                        if ($user->user_gender == 'Female') {
                                                            echo "selected='selected'";
                                                        }
                                                        ?>>Female</option>
                                                    </select>
                                                    <label for="user_gender">Gender</label>
                                                </div>
                                            </div><!--end .col -->
                                        </div><!--end .row -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <select class="form-control" name="role_id" id="role_id">
                                                        <option value="">Select</option>
                                                        <?php foreach ($roles as $role) { ?>
                                                            <option value="<?php echo $role->id; ?>" <?php
                                                            if ($role->id == $user->role) {
                                                                echo "selected='selected'";
                                                            }
                                                            ?>><?php echo ucfirst($role->name); ?></option>
                                                                <?php } ?>
                                                    </select>
                                                    <label for="role_id">Assign Role</label>
                                                </div>
                                            </div><!--end .col -->
                                        </div><!--end .row -->
                                    </div><!--end .col -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div id="img-canvas" class="border-gray height-7">
                                                <?php if ($user->user_pic != '' && $user->user_pic != null) : ?>
                                                    <img src="<?php echo base_url() . 'uploads/users/profile/' . $user->user_pic; ?>" width="250px">
                                                <?php else: ?>
                                                    <img src="<?php echo base_url(); ?>/assets/img/default-user.jpg" alt="" width="250px"/>
                                                <?php endif; ?>
                                            </div>
                                            <div  class="checkbox checkbox-styled pull-right"><label><input name="deleteProfileimage" type="checkbox" id="deleteProfileimage" value="1" > Delete Current Image</label></div>
                                            <div class="form-group">
                                                <input class="form-control" type="file" name="user_pic" id="user_pic">
                                            </div>
                                        </div>
                                    </div><!--end .col -->
                                </div><!--end .row -->
                            </div><!--end .tab-pane -->
                            <div class="tab-pane" id="general">
                                <div class="form-group">
                                    <textarea id="summernote" name="note" class="form-control control-4-rows" placeholder="Enter note ..." spellcheck="false"><?php echo $user->note; ?></textarea>
                                </div><!--end .form-group -->
                            </div><!--end .tab-pane -->
                        </div><!--end .card-body.tab-content -->
                        <!-- END FORM TAB PANES -->

                        <!-- BEGIN FORM FOOTER -->
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <button type="submit" class="btn btn-flat btn-accent">Update Profile</button>
                            </div><!--end .card-actionbar-row -->
                        </div><!--end .card-actionbar -->
                        <!-- END FORM FOOTER -->

                    </form>
                </div><!--end .card -->
            </div><!--end .col -->
            <!-- END ADD CONTACTS FORM -->

        </div><!--end .row -->
    </div><!--end .section-body -->
</section>

