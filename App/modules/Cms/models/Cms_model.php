<?php

/*

 */

class Cms_model extends CI_Model {

   public $table_cms = 'cms';
    public $table_static_block = 'static_blocks';
    var $pages_table = "pages";
    var $rule_table = "nc_rules";

    function __construct() {
        parent::__construct();
    }

    public function saveBlocks() {
        $action = $this->input->get('S');
        $alias = $this->input->post('section');

        $data['content'] = "";
        switch ($action) {
            case 'f-cms':
                $pids = $this->input->post('flinks');
                if (!empty($pids))
                    $data['content'] = serialize(explode(',', $pids));
                break;
            case 'logo':
                $this->deletelogo();
                $logo_info = $this->uploadLogo();
                if ($logo_info['file_name']) {
                    $data['content'] = $logo_info['file_name'];
                }
                break;

            default:
                $data['content'] = serialize($this->input->post());
                break;
        }
        $this->db->where('alias', $alias);

        $return = $this->db->update($this->table_cms, $data);
        return $return;
    }

    public function getStaicBlocks($alias = "") {
        if ($alias != "")
            $this->db->where('alias', $alias);

        $query = $this->db->get($this->table_cms);
        $qqq = $query->result();
        $ret = array();
        foreach ($qqq as $row) {
            $ret[$row->alias] = $row;
        }
        return $ret;
    }

    function uploadLogo() {
        $config = array();
        $config['upload_path'] = IMAGESPATH . 'logo/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['file_name'] = md5(uniqid(rand(), true));
        $this->load->helper(array('form', 'url'));
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('logo')) {
            $info = $this->upload->data();
        } else {
            $info = array('file_name' => '');
        }
        return $info;
    }

    function deletelogo() {
        if ($this->input->post('deleteLogo')) {
            $logo = $this->getStaicBlocks();
            $file_name = $logo['logo']->content;
            unlink(IMAGESPATH . 'logo/' . $file_name);
        }
    }

    function getList($showPerpage, $offset) {
        $conditions = array(
            'is_deleted' => '0'
        );
        $this->db->where($conditions);
        $this->db->limit($showPerpage, $offset);
        $this->db->from($this->pages_table);
        $result = $this->db->get();
        return $result->result();
    }

    function updateStatus() {
        if ($this->input->post('Delete') != null) {
            $data = array(
                'is_deleted' => '1'
            );
        } else {
            if ($this->input->post('Deactivate') != null) {
                $status = "0";
            } else if ($this->input->post('Activate') != null) {
                $status = "1";
            }
            $data = array(
                'is_active' => $status
            );
        }
        $arr_ids = $this->input->post('arr_ids');
        if (!empty($arr_ids)) {
            $this->db->where_in('id', $arr_ids);
            $this->db->update($this->pages_table, $data);
        }
    }

    function count_all() {
        $conditions = array(
            'is_deleted' => '0'
        );
        $this->db->where($conditions);
        $query = $this->db->get($this->pages_table);
        return $query->num_rows();
    }

    function delete($id) {
        $data = array(
            'is_deleted' => '1',
        );
        $this->db->where('id', $id);
        $query = $this->db->update($this->pages_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function create() {
        $data = $this->input->post();
        $query = $this->db->insert($this->pages_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    function update($id) {
        $data = $this->input->post();
        $this->db->where('id', $id);
        $query = $this->db->update($this->pages_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    function get($id) {
        $this->db->where('id', $id);
        $this->db->from($this->pages_table);
        $result = $this->db->get();
        return $result->row();
    }

    function getAllParentPage($exclue = 0) {
        if ($exclue != 0)
            $this->db->where('id !=', $exclue);
        $this->db->where('parent_id', '0');
        $this->db->order_by("id", "desc");
        $query = $this->db->get($this->pages_table);
        return $query->result();
    }

    function getContent($alias) {
        $this->db->where('alias', $alias);
        $query = $this->db->get($this->pages_table);
        return $query->row();
    }

    public function getRules() {
        $query = $this->db->get($this->rule_table);
        return $query->result();
    }

    function getRule($id) {
        $this->db->where('rule_id', $id);
        $this->db->from($this->rule_table);
        $result = $this->db->get();
        return $result->row();
    }

    function updateRule($id) {
        $data = array(
            'description' => $this->input->post('description')
        );
        $this->db->where('rule_id', $id);
        $query = $this->db->update($this->rule_table, $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

}

?>