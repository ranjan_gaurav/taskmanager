<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions

/**
 * @author          Orange Mantra
 * @license         OM
 */
class Cms extends Admin_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('cms_model');
        $this->load->library("session");
        $this->load->library('pagination');
    }

    

    public function getSitetitle() {
        $info = $this->cms_model->getStaicBlocks();
        return $info['contacts']->content;
    }

    

    function page($slug = null) {
        $alias = $this->uri->segment(1);
        $pageAlias = str_replace('.html', '', $alias);
        $data['result'] = $this->cms_model->getContent($pageAlias);
        $data['meta_title'] = $data['result']->meta_title;
        $data['meta_description'] = $data['result']->meta_description;
        $data['meta_keyword'] = $data['result']->meta_keywords;
        $data['main_content'] = 'pages';
        $this->load->view('full_width_banner', $data);
    }

    function contact_us() {
        $includeJs = array(
            'theme/js/contact.js',
            'theme/js/bootbox.min.js',
            'theme/plugins/jquery-validation/lib/jquery.js',
            'theme/plugins/jquery-validation/dist/jquery.validate.js'
        );
        $includeCss = array('theme/plugins/jquery-validation/demo/css/screen.css', 'theme/plugins/contact-form/bootstrap/css/contact.css', 'theme/plugins/contact-form/bootstrap/css/bootstrap.min.css', 'theme/plugins/contact-form/font-awesome/css/font-awesome.min.css');
        $data['includes_for_layout_css'] = add_includes('css', $includeCss);
        $data['includes_for_layout_js'] = add_includes('js', $includeJs, 'footer');
        $data['meta_description'] = 'Contact Us';
        $data['meta_keyword'] = 'Contact Us';
        $data['main_content'] = "contact";
        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
        if ($this->form_validation->run() == false) {
            $data['error'] = validation_errors();
        } else {
            $message='<table>'
                    .'<tr><td>Name:</td><td>'.$this->input->post('first_name').'-'.$this->input->post('last_name').'</td>'
                    .'<tr><td>Phone:</td><td>'.$this->input->post('phone').'</td>'
                    .'<tr><td>Email:</td><td>'.$this->input->post('email').'</td>'
                    .'<tr><td>Message:</td><td>'.$this->input->post('message').'</td>'
                    . '</table>';
            $this->load->library('email');
            $this->email->from($this->input->post('email'), $this->input->post('first_name'));
            $this->email->to(ADMIN_EMAIL);
            //$this->email->cc('another@another-example.com');
            $this->email->subject('99 Retail Street Contact Form');
            $this->email->message($message);
            $this->email->send();

            $this->session->set_flashdata('message', 'Thank You for the message.We will contact you soon!');
        }
        $data['meta_title'] = 'Contact Us';
        $this->load->view('full_width_banner', $data);
    }
    
    
}
