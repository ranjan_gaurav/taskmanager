<?php

/*
 *
 */
class Job_model extends CI_Model {
	var $table = "category";
	var $subcat= "subcategory";
	var $app = "application";
	var $query = "query";
	var $original_path;
	var $resized_path;
	var $thumbs_path;
	function __construct() {
		parent::__construct ();
		error_reporting(0);
		$this->load->library ( 'session' );
		$this->load->library ( 'pagination' );
	}
	function read($showPerpage, $offset) {
		$id = $this->GetAppID($this->session->userdata('userid'));
		$this->db->limit ( $showPerpage, $offset );
		$this->db->where('app_id',$id);
		$this->db->select ( 'category.*' );
		$this->db->from ( $this->table );
		//$this->db->join ( 'qualifications', 'qualifications.qualification_id = jobs.qualification_id', 'left' );
		$query = $this->db->get ();
		// echo $this->db->last_query();die;
		return $query->result ();
	}
	
	function read_query($showPerpage, $offset) {
		$id = $this->session->userdata('app_id');
		$this->db->limit ( $showPerpage, $offset );
		$this->db->where('app_id',$id);
		$this->db->select ( 'query.*' );
		$this->db->from ( $this->query );
		//$this->db->join ( 'qualifications', 'qualifications.qualification_id = jobs.qualification_id', 'left' );
		$query = $this->db->get ();
	//	 echo $this->db->last_query();die;
		return $query->result ();
	}
	function read_apps($showPerpage, $offset,$id) {
		
		if($id == 1)
		{
		
		$this->db->limit ( $showPerpage, $offset );
		$this->db->select ( 'application.*' );
		$this->db->from ( $this->app );
		$query = $this->db->get ();
		}
		
		else {
			$this->db->where('userid',$id);
			$this->db->limit ( $showPerpage, $offset );
			$this->db->select ( 'application.*' );
			$this->db->from ( $this->app );
			$query = $this->db->get ();
		}
		
		return $query->result ();
	}
	function read_subcategory($showPerpage, $offset) {
		$id = $this->GetAppID($this->session->userdata('userid'));
		$this->db->limit ( $showPerpage, $offset );
		$this->db->where('app_id',$id);
		$this->db->select ( 'subcategory.*' );
		$this->db->from ( $this->subcat );
		//$this->db->join ( 'qualifications', 'qualifications.qualification_id = jobs.qualification_id', 'left' );
		$query = $this->db->get ();
		// echo $this->db->last_query();die;
		return $query->result ();
	}
	function usersFeilds($showPerpage, $offset) {
		$this->db->limit ( $showPerpage, $offset );
		$this->db->from ( $this->usersFeilds );
		$query = $this->db->get ();
		return $query->result ();
	}
	function count_all() {
		$query = $this->db->get ( $this->table );
		return $query->num_rows ();
	}
	function count_all_apps() {
		$query = $this->db->get ( $this->app );
		return $query->num_rows ();
	}
	function jobs_by_id($jobid) {
		$this->db->where ('category_id', $jobid );
		$query = $this->db->get ( $this->table );
		if ($query->num_rows () > 0) {
			return $query->row ();
		} else {
			return false;
		}
	}
	
	function sub_cat_by_id($jobid) {
		$this->db->where ('sub_cat_id', $jobid );
		$query = $this->db->get ( $this->subcat );
		if ($query->num_rows () > 0) {
			return $query->row ();
		} else {
			return false;
		}
	}
	function job_update($id) {
		$data ['category_name'] = $this->input->post ( 'title', true );
		$data ['category_description'] = $this->input->post ( 'jobdescription', true );
		$data ['status'] = '1';
		//$data ['app_id'] = $this->GetAppID($this->session->userdata('userid'));
		$this->db->where ( 'id', $id );
		$query = $this->db->update ( $this->table, $data );
		if ($query) {
			return true;
		} else {
			return false;
		}
	}
	function create_job() {
		
		$data ['category_name'] = $this->input->post ( 'title', true );
		$data ['category_description'] = $this->input->post ( 'jobdescription', true );
		$data ['status'] = '1';
		$data ['app_id'] = $this->GetAppID($this->session->userdata('userid'));
		
		//print_r($data ['app_id']); die();
		
		$this->db->set ( 'start_date', 'NOW()', false );
		$query = $this->db->insert ( $this->table, $data );
		if ($query) {
			$user_id = $this->db->insert_id ();
			// $user_value = $this->input->post('role_id') ? $this->input->post('role_id') : '3';
			// $this->update_role($user_id, $user_value);
			return true;
		} else {
			return false;
		}
	}

	function create_sub_cat($val) {
	
	
		$this->db->set ( 'start_date', 'NOW()', false );
		$query = $this->db->insert ( $this->subcat, $val );
		//echo $this->db->last_query(); die();
		if ($query) {
			$user_id = $this->db->insert_id ();
			// $user_value = $this->input->post('role_id') ? $this->input->post('role_id') : '3';
			// $this->update_role($user_id, $user_value);
			return true;
		} else {
			return false;
		}
	}
	
	

	function updateStatus() {
		if ($this->input->post ( 'Delete' ) != null) {
			$arr_ids = $this->input->post ( 'arr_ids' );
			if (! empty ( $arr_ids )) {
				$this->db->where_in ( 'category_id', $arr_ids );
				$this->db->delete ( $this->table );
			}
		} else {
			if ($this->input->post ( 'Deactivate' ) != null) {
				$status = "0";
			} else if ($this->input->post ( 'Activate' ) != null) {
				$status = "1";
			}
			$data = array (
					'status' => $status 
			);
			
			$arr_ids = $this->input->post ( 'arr_ids' );
			if (! empty ( $arr_ids )) {
				$this->db->where_in ( 'category_id', $arr_ids );
				$this->db->update ( $this->table, $data );
			}
		}
	}
	
	function update_subcat_Status() {
		if ($this->input->post ( 'Delete' ) != null) {
			$arr_ids = $this->input->post ( 'arr_ids' );
			if (! empty ( $arr_ids )) {
				$this->db->where_in ( 'sub_cat_id', $arr_ids );
				$this->db->delete ( $this->subcat );
			}
		} else {
			if ($this->input->post ( 'Deactivate' ) != null) {
				$status = "0";
			} else if ($this->input->post ( 'Activate' ) != null) {
				$status = "1";
			}
			$data = array (
					'status' => $status
			);
				
			$arr_ids = $this->input->post ( 'arr_ids' );
			if (! empty ( $arr_ids )) {
				$this->db->where_in ( 'sub_cat_id', $arr_ids );
				$this->db->update ( $this->subcat, $data );
			}
		}
	}
	
	function update_app_Status() {
		if ($this->input->post ( 'Delete' ) != null) {
			$arr_ids = $this->input->post ( 'arr_ids' );
			if (! empty ( $arr_ids )) {
				$this->db->where_in ( 'app_id', $arr_ids );
				$this->db->delete ( $this->app );
			}
		} else {
			if ($this->input->post ( 'Deactivate' ) != null) {
				$status = "0";
			} else if ($this->input->post ( 'Activate' ) != null) {
				$status = "1";
			}
			$data = array (
					'status' => $status
			);
	
			$arr_ids = $this->input->post ( 'arr_ids' );
			if (! empty ( $arr_ids )) {
				$this->db->where_in ( 'app_id', $arr_ids );
				$this->db->update ( $this->app, $data );
			}
		}
	}
	function get_categories() {
		$id = $this->session->userdata('app_id');
		$this->db->where('app_id',$id);
		$this->db->from ( $this->table );
		$result = $this->db->get ();
		return $result->result ();
	}
	
	public function sub_cat_update($id)
	{
		$file = $this->UploadProfilePhoto();
		if ($file['file_name'] != '') {
			$data['user_pic'] = $file['file_name'];
		}
		
	    $data['sub_cat_name'] = $this->input->post('sub_cat_name');
		$data['sub_cat_description'] = $this->input->post('sub_cat_desc');
		 
		
		
	    $this->db->where ( 'sub_cat_id', $id );
		$query = $this->db->update ( $this->subcat, $data );
		if ($query) {
			return true;
		} else {
			return false;
		}
	}
	
	public function UploadProfilePhoto() {
	
		$config = array();
		ini_set('upload_max_filesize', '200M');
		ini_set('post_max_size', '200M');
		ini_set('max_input_time', 6000);
		ini_set('max_execution_time', 6000);
		$config['upload_path'] = IMAGESPATH . 'users/profile/';
	
		//print_r($config['upload_path']); die();
	
		$config['allowed_types'] = '*';
		$config['file_name'] = md5(uniqid(rand(), true));
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ($this->upload->do_upload('user_pic')) {
			$info = $this->upload->data();
			return $info;
		}
	}

	public function GetAppID($ID)
	{
		$this->db->where('userid',$ID);
		$this->db->select('app_id');
		$this->db->from ( 'application' );
		$query = $this->db->get ();
		// echo $this->db->last_query();die;
		return $query->row ()->app_id;
	
	}
}

?>