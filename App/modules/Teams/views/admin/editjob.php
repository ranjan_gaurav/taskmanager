
<section>
    <div class="contain-lg">
        <!-- BEGIN BASIC VALIDATION -->
        <div class="row">
            <div class="col-md-12">
                <form class="form form-validate floating-label" novalidate="novalidate" method="post">
                    <div class="card-head style-primary">
                        <div class="tools pull-left">
                            <header>Edit Page</header>
                        </div>
                        <div class="tools">
                            <a class="btn btn-flat hidden-xs" href="<?php echo base_url('admin/Jobs'); ?>"><span class="glyphicon glyphicon-arrow-left"></span> &nbsp;Back</a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <?php if (@$error): ?>
                                <div class="alert alert-callout alert-warning" role="alert">
                                    <strong>Warning!</strong> <?php echo $error; ?>
                                </div>
                            <?php endif; ?>
                          
                            <div class="form-group">
                                    <input type="text" class="form-control" id="title" name="title" required data-rule-minlength="2" value ="<?php echo $job->category_name;?>" />
                                    <label for="meta_description">Title of Category</label>
                                </div>

                           
                                                           
                               <div class="form-group">
                                     <textarea name="jobdescription" id="jobdescription" class="form-control" rows="5" columns ="4"><?php echo $job->category_description;?></textarea> 
                                    <label for="title">Job Description</label>
                               </div>    
                                
                                
                        </div><!--end .card-body -->
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <button type="submit" class="btn ink-reaction btn-raised btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Loading...">Update<div style="top: 26px; left: 32.5px;" class="ink"></div></button>
                            </div>
                        </div><!--end .card-actionbar -->
                    </div><!--end .card -->
                </form>
            </div><!--end .col -->
        </div><!--end .row -->
        <!-- END BASIC VALIDATION -->

    </div><!--end .section-body -->
</section>