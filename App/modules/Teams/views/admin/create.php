<script
	src="<?php echo base_url('assets/plugins/ckeditor/ckeditor.js'); ?>"></script>
<script type="text/javascript"
	src="<?php echo base_url('assets/plugins/ckfinder/ckfinder.js'); ?>"></script>
	<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
<section style="padding: 0px;">
	<div class="contain-lg">
		<!-- BEGIN BASIC VALIDATION -->
		<div class="row">
			<div class="col-md-12">
				<div class="card-body form-inverse">
					<form class="form form-validate floating-label"
						novalidate="novalidate" method="post">
						<div class="card-head style-primary">
							<div class="tools pull-left">
								<header>Add New Category</header>
							</div>
							<div class="tools">
								<a class="btn btn-flat hidden-xs"
									href="<?php echo base_url('admin/Jobs'); ?>"><span
									class="glyphicon glyphicon-arrow-left"></span> &nbsp;Back</a>
							</div>
						</div>
						<div class="card">
							<div class="card-body">
                                <?php if (@$error): ?>
                                    <div
									class="alert alert-callout alert-warning" role="alert">
									<strong>Warning!</strong> <?php echo $error; ?>
                                    </div>
                                <?php endif; ?>
<!--                                 <div class="form-group"> -->
								<!--                                     <textarea name="title" id="textarea1" class="form-control" rows="3" required></textarea> -->
								<!--                                     <label for="title">Blog Title</label> -->
								<!--                                 </div> -->

								<!--                                 <div class="form-group"> -->
								<!--                                     <textarea id="editor1" class="form-control control-5-rows" placeholder="Enter text ..." name="description"> -->
								<!--                                     </textarea> -->

								
								<div class="form-group">
									<input type="text" class="form-control" id="title" name="title"
										required data-rule-minlength="2"> <label
										for="meta_description">Title of Category</label>
								</div>
								

								<div class="form-group">
									<textarea name="jobdescription" id="jobdescription"
										class="form-control" rows="5" columns="4"></textarea>
									<label for="title">Category Description</label>
								</div>





							</div>
							<!--end .card-body -->
							<div class="card-actionbar">
								<div class="card-actionbar-row">
									<button type="submit" id="submit"
										class="btn ink-reaction btn-raised btn-primary btn-loading-state"
										data-loading-text="<i  class='fa fa-spinner fa-spin'>
										</i> Loading...">Create New Category
										<div style="top: 26px; left: 32.5px;" class="ink"></div>
									</button>
								</div>
							</div>
							<!--end .card-actionbar -->
						</div>
						<!--end .card -->
					</form>
				</div>
			</div>
			<!--end .col -->
		</div>
		<!--end .row -->
		<!-- END BASIC VALIDATION -->

	</div>
	<!--end .section-body -->
</section>

<script type="text/javascript">
function initialize() {
    var input = document.getElementById('locations');
    var options = {componentRestrictions: {country: 'in'}};
                 
    new google.maps.places.Autocomplete(input, options);
}
             
google.maps.event.addDomListener(window, 'load', initialize);
</script>


