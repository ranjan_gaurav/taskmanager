
<?php $id = $this->session->userdata('userid'); ?>
<section class="style-default-bright">
    <div class="section-header">
        <div class="col-md-12">
            <div class="col-sm-6 col-md-6">
               <h2 class="text-primary">Applications</h2>
            </div>
           
        </div><!--end .col -->
    </div>
    <?php if ($this->session->flashdata('message')) { ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('message'); ?>
        </div>
    <?php } ?>
    <?php if ($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Danger!</strong> <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php } ?>
    <div class="section-body">

        <!-- BEGIN DATATABLE 1 -->
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-6 col-md-12">
                    <h4>Apps</h4>
                </div>
            </div><!--end .col -->
            <form method="post" name="form1" id="form1" action="">
                <div class="col-lg-12">
                    <div class="table-responsive">

                        <table id="datatable1" class="table table-striped table-hover">
                            <thead>
                                <tr>
                            <th><div class="checkbox checkbox-styled"><label><input name="check_all" type="checkbox" id="check_all" value="1" onclick="checkall(this.form)" ></label></div></th>
                            <th>ApplicationID</th>
                            <?php if($id == 1){?>
                            <th>UserID</th><?php }?>
                            <th>ApplicationTitle</th>
                            <th>ApplicationName</th>
                            <th>Status</th>
                            
                            
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($apps as $app): ?>
                                    <tr class="<?php echo ($app->status == 'active' ? 'gradeX' : 'gradeA'); ?>">
                                        <td>
                                            <div class="checkbox checkbox-styled"><label><input name="arr_ids[]" type="checkbox" id="arr_ids[]" value="<?php echo $app->app_id; ?>" ></label></div>
                                        </td>
                                        <td><?php echo $app->app_id ;?></td>
                                        <?php if($id ==1){?>
                                        <td><?php echo $app->userid;?></td><?php }?>
                                        <td><?php echo $app->app_title; ?></td>
                                        <td><?php echo $app->app_name; ?></td>
                                        <td><?php echo ($app->status == "1" ? "active":"Inactive");?></td>
                
                                       
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div><!--end .table-responsive -->
                </div><!--end .col -->
                <?php if($id == 1){?>
                <div class="col-sm-12 col-md-12">
                    <div class="col-sm-6 col-md-6">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="left" style="padding:2px">
                                    <input type="submit" name="Activate" value="Activate" class="btn"/>
                                    <input type="submit" name="Deactivate" value="Deactivate" class="btn" />
                                    <input type="submit" name="Delete" value="Delete" class="btn" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-6 col-md-6 pull-right">
                        <?php //echo $links; ?>
                    </div>
                </div>
                <?php }?>
            </form>

        </div><!--end .row -->
        <!-- END DATATABLE 1 -->
        <hr class="ruler-xxl"/>


    </div><!--end .section-body -->
</section>
