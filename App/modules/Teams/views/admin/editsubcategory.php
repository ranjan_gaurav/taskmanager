<section>
    <div class="section-body contain-lg">
        <div class="row">

            <!-- BEGIN ADD CONTACTS FORM -->
            <div class="col-md-12">
                <div class="card">
                    <div class="card-head style-primary">
                        <div class="tools pull-left">
                            <header>Edit Post : </header>
                        </div>
                        <div class="tools">
                            <a class="btn btn-flat hidden-xs" href="<?php echo base_url('admin/Jobs/subcategory'); ?>"><span class="glyphicon glyphicon-arrow-left"></span> &nbsp;Back</a>
                        </div>
                    </div>
                    <form class="form" role="form" method="post" action="" id="userForm" enctype="multipart/form-data">

                        <!-- BEGIN DEFAULT FORM ITEMS -->
                        <?php if (@$error): ?>
                            <div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <?php echo $error; ?>
                            </div>
                        <?php endif; ?>
                        <?php if ($this->session->flashdata('message')) { ?>
                            <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>Success ! </strong> <?php echo $this->session->flashdata('message'); ?>
                            </div>
                        <?php } ?>
                      
                        <!-- END DEFAULT FORM ITEMS -->

                        <!-- BEGIN FORM TABS -->
                        <div class="card-head style-primary">
                            <ul class="nav nav-tabs tabs-text-contrast tabs-accent" data-toggle="tabs">
                                <li class="active"><a href="#contact"></a></li>
<!--                                 <li><a href="#general">Notes</a></li> -->
                            </ul>
                        </div><!--end .card-head -->
                        <!-- END FORM TABS -->

                        <!-- BEGIN FORM TAB PANES -->
                        <div class="card-body tab-content">
                            <div class="tab-pane active" id="contact">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-6">
                                                 <div class="form-group">
                                                    <input type="text" class="form-control" id="sub_cat_name" value="<?php echo $job->sub_cat_name; ?>" name="sub_cat_name" required>
                                                    <label for="title">Title of SubCategory</label>
                                                </div>
                                            </div><!--end .col -->
                                          
                                        </div><!--end .row -->
                                      
                                         <div class="form-group">
									<textarea name="sub_cat_desc" id="sub_cat_desc"
										class="form-control" rows="5" columns="4"><?php echo $job->sub_cat_description;?></textarea>
									<label for="title">SubCategory Description</label>
								</div>
                                        </div><!--end .row -->
                                      
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div id="img-canvas" class="border-gray height-7">
                                                <?php if ($job->user_pic != '' && $job->user_pic != null) : ?>
                                                    <img src="<?php echo base_url() . 'uploads/users/profile/' . $job->user_pic; ?>" width="250px">
                                                <?php else: ?>
                                                    <img src="<?php echo base_url(); ?>/assets/img/default-user.jpg" alt="" width="250px"/>
                                                <?php endif; ?>
                                            </div>
                                            <div  class="checkbox checkbox-styled pull-right"><label><input name="deleteProfileimage" type="checkbox" id="deleteProfileimage" value="1" > Delete Current Image</label></div>
                                            <div class="form-group">
                                                <input class="form-control" type="file" name="user_pic" id="user_pic">
                                            </div>
                                        </div>
                                    </div><!--end .col -->
                                </div><!--end .row -->
                            </div><!--end .tab-pane -->
                            <div class="tab-pane" id="general">
                              
                            </div><!--end .tab-pane -->
                        </div><!--end .card-body.tab-content -->
                        <!-- END FORM TAB PANES -->

                        <!-- BEGIN FORM FOOTER -->
                        <div class="card-actionbar">
                            <div class="card-actionbar-row">
                                <button type="submit" class="btn btn-flat btn-accent">Update</button>
                            </div><!--end .card-actionbar-row -->
                        </div><!--end .card-actionbar -->
                        <!-- END FORM FOOTER -->

                    </form>
                </div><!--end .card -->
            </div><!--end .col -->
            <!-- END ADD CONTACTS FORM -->

        </div><!--end .row -->
    </div><!--end .section-body -->
</section>

