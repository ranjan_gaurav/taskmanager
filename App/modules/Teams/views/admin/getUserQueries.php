
<section class="style-default-bright">
    <div class="section-header">
        <div class="col-md-12">
            <div class="col-sm-6 col-md-6">
               <h2 class="text-primary">User Queries</h2>
            </div>
           
        </div><!--end .col -->
    </div>

    <div class="section-body">

        <!-- BEGIN DATATABLE 1 -->
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-6 col-md-12">
                    <h4>queries</h4>
                </div>
            </div><!--end .col -->
            <form method="post" name="form1" id="form1" action="">
                <div class="col-lg-12">
                    <div class="table-responsive">

                        <table id="datatable1" class="table table-striped table-hover">
                            <thead>
                                <tr>
                            <th><div class="checkbox checkbox-styled"><label><input name="check_all" type="checkbox" id="check_all" value="1" onclick="checkall(this.form)" ></label></div></th>
                            <th>Categoryid</th>
                            <th>UserId</th>
                            <th>Description</th>
                            <th>CreatedDate</th>
                           
                             </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($jobs as $job): ?>
                                    <tr class="<?php echo ($job->status == 'active' ? 'gradeX' : 'gradeA'); ?>">
                                        <td>
                                            <div class="checkbox checkbox-styled"><label><input name="arr_ids[]" type="checkbox" id="arr_ids[]" value="<?php echo $job->cat_id; ?>" ></label></div>
                                        </td>
                                        <td><?php echo $job->cat_id; ?></td>             
                                        <td><?php echo $job->user_id; ?></td>
                                        <td><?php echo $job->query_desc; ?></td>
                                        <td><?php echo $job->created_date;?></td>
                                        
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div><!--end .table-responsive -->
                </div><!--end .col -->

                <div class="col-sm-12 col-md-12">
                    <div class="col-sm-6 col-md-6">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="left" style="padding:2px">
                                    <input type="submit" name="Activate" value="Activate" class="btn"/>
                                    <input type="submit" name="Deactivate" value="Deactivate" class="btn" />
                                    <input type="submit" name="Delete" value="Delete" class="btn" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-6 col-md-6 pull-right">
                        <?php //echo $links; ?>
                    </div>
                </div>
            </form>

        </div><!--end .row -->
        <!-- END DATATABLE 1 -->
        <hr class="ruler-xxl"/>


    </div><!--end .section-body -->
</section>
