<?php

/**
 * Created by PhpStorm.
 * User: Gaurav Ranjan<ranjan.gaurav@orangemantra.in>
 * Date: 15/01/2016
 * Time: 11:28 AM
 */
class User_model extends CI_Model {
	var $user_table = "users";
	var $device_table = "device_info";
	function __construct() {
		parent::__construct ();
	}
	
	/**
	 * ription : Get User data
	 *
	 * @param null $ID        	
	 * @return mixed data
	 *         @date : 04/12/2015
	 */
	public function getAllUsers($ID = null) {
		if ($ID != null) {
			$this->db->where ( 'users.user_id!=', $ID );
		}
		$this->db->where ( 'status', '1' );
		$this->db->select ( 'users.*,team.team_title,team.team_id,device_info.fcmId,if(user_pic="","null" ,CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",user_pic ) ) user_pic' );
		$this->db->from ( $this->user_table );
		$this->db->join ( 'team_member', 'team_member.user_id = users.user_id', 'left' );
		$this->db->join ( 'team', 'team.team_id = team_member.team_id' );
		$this->db->join ( 'device_info', 'device_info.user_id = users.user_id', 'left' );
		$result = $this->db->get ();
		return $result->result ();
	}
	public function get_details_result($user_id) {
		$this->db->where ( 'care_id', $user_id );
		$this->db->select ( 'id,user_pic' );
		$this->db->from ( $this->user_table );
		$result = $this->db->get ();
		return $result->result ();
	}
	
	/**
	 * ription : Add new User in the User table
	 *
	 * @method : Add User
	 * @param
	 *        	$data
	 * @return bool @date : 04/12/2015
	 */
	public function create($data) {
		try {
			
			$this->load->library ( 'form_validation' );
			$config = array (
					array (
							'field' => 'email',
							'label' => 'Email',
							'rules' => 'trim|required|is_unique[users.email]' 
					),
					array (
							'field' => 'mobile',
							'label' => 'Mobile',
							'rules' => 'trim|required|numeric' 
					) 
			);
			
			$this->form_validation->set_rules ( $config );
			$this->form_validation->set_message ( 'is_unique', 'You are already a registered member. Please Sign-in or click on forgot password.' );
			if ($this->form_validation->run () == false) {
				$errors_array = '';
				foreach ( $config as $row ) {
					$field = $row ['field'];
					$error = strip_tags ( form_error ( $field ) );
					if ($error)
						$errors_array .= $error . ', ';
				}
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => rtrim ( $errors_array, ', ' ) 
				);
			} else {
				$this->db->set ( 'registered_date', 'NOW()', false );
				$this->db->set ( 'user_modification_date', 'NOW()', false );
				$this->db->insert ( $this->user_table, $data );
				$insert_id = $this->db->insert_id ();
				
				$message = array (
						'status' => true,
						'response_code' => '1',
						'message' => "User Added Successfully",
						'userid' => $insert_id 
				);
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		return $message;
	}
	
	/**
	 * ription : To update User
	 *
	 * @param
	 *        	$id
	 * @param
	 *        	$data
	 * @return bool @date : 20/01/2016
	 * @method : Update User
	 */
	public function update($id, $data) {
		try {
			$update_data = $data;
			
			// print_r($update_data);
			// exit;
			
			$this->db->set ( 'user_modification_date', 'NOW()', FALSE );
			$this->db->where ( 'id', $id );
			$query = $this->db->update ( $this->user_table, $update_data );
			if ($query) {
				$message = array (
						'status' => true,
						'response_code' => '1',
						'message' => "Success" 
				);
			} else {
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => 'Something Went wrong!' 
				);
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		return $message;
	}
	
	/**
	 * ription : To Upload Profile Pic
	 *
	 * @param
	 *        	$id
	 * @return bool @date : 19/01/2016
	 * @method : Upload Profile Pic
	 */
	protected function upload_profile_pic() {
		$config = array ();
		ini_set ( 'upload_max_filesize', '200M' );
		ini_set ( 'post_max_size', '200M' );
		ini_set ( 'max_input_time', 3000 );
		ini_set ( 'max_execution_time', 3000 );
		$config ['upload_path'] = IMAGESPATH . 'users/profile/';
		$config ['allowed_types'] = '*'; // 'mp3|wav';
		$config ['file_name'] = md5 ( uniqid ( rand (), true ) );
		$this->load->library ( 'upload', $config );
		$this->upload->initialize ( $config );
		if ($this->upload->do_upload ( 'photo_id' )) {
			$info = $this->upload->data ();
			return $info;
		}
	}
	
	/**
	 * ription : To delete User from User table
	 *
	 * @param
	 *        	$id
	 * @return bool @date : 04/12/15
	 * @method : Delete User
	 */
	public function delete($id) {
		if (isset ( $id ) && $id != '') {
			$this->db->where ( 'id', $id );
			$query = $this->db->delete ( $this->user_table );
			if ($query) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	function validate($mobile, $password) {
		$cond = array (
				'user_email' => $mobile,
				'password' => $password 
		);
		$this->db->where ( $cond );
		$this->db->select ( 'users.*,roles.name as rolename' );
		$this->db->from ( 'users' );
		$this->db->join ( 'roles', 'roles.id = users.role_id' );
		$query = $this->db->get ();
		
		if ($query->num_rows () === 1) {
			return $query->row ();
		} else {
			return false;
		}
	}
	public function updateDeviceData($userId, $deviceType, $deviceId, $fcmRegId, $IosID) {
		$query = "SELECT * FROM device_info WHERE user_id = " . $this->db->escape ( $userId );
		$result = $this->db->query ( $query );
		
		if ($result->num_rows () > 0) {
			// update
			$query = "UPDATE device_info SET fcmId=" . $this->db->escape ( $fcmRegId ) . ", reg_id_ios=" . $this->db->escape ( $IosID ) . ", deviceType=" . $this->db->escape ( $deviceType ) . ", modificationDate=now() WHERE user_id=" . $this->db->escape ( $userId );
			$result = $this->db->query ( $query ); // echo $this->db->last_query(); die();
			if ($result) {
				return true;
			} else {
				return false;
			}
		} else {
			// insert
			$query = "INSERT INTO device_info (`deviceId`, `deviceType`, `fcmId`,reg_id_ios, `user_id`)  VALUES (" . $this->db->escape ( $deviceId ) . "," . $this->db->escape ( $deviceType ) . "," . $this->db->escape ( $fcmRegId ) . "," . $this->db->escape ( $IosID ) . "," . $this->db->escape ( $userId ) . ")";
			$result = $this->db->query ( $query ); // echo $this->db->last_query(); die();
			if ($result) {
				return true;
			} else {
				return false;
			}
		}
	}
	function CheckCareDevice($deviceId, $userId, $deviceType) {
		$this->db->where ( array (
				'device_id' => $deviceId,
				'user_id' => $userId,
				'device_type' => $deviceType 
		) );
		$res = $this->db->get ( $this->device_table );
		return $res->num_rows ();
	}
	function CheckDevice($deviceId, $deviceType) {
		$this->db->where ( array (
				'device_id' => $deviceId,
				'device_type' => $deviceType 
		) );
		$res = $this->db->get ( $this->device_table );
		return $res->num_rows ();
	}
	public function updateDeviceId($deviceId, $careId, $regId) {
		$data = array (
				'reg_id' => $regId 
		);
		$this->db->where ( array (
				'device_id' => $deviceId,
				'user_id' => $careId 
		) );
		$res = $this->db->update ( $this->device_table, $data );
		if ($res) {
			return true;
		} else {
			return false;
		}
	}
	public function InsertDeviceId($deviceId, $userId, $regId, $deviceType) {
		$data = array (
				'device_id' => $deviceId,
				'user_id' => $userId,
				'reg_id' => $regId,
				'device_type' => $deviceType 
		);
		$res = $this->db->insert ( $this->device_table, $data );
		if ($res) {
			return true;
		} else {
			return false;
		}
	}
	function DeleteDeviceUser($deviceId) {
		$this->db->where ( array (
				'device_id' => $deviceId 
		) );
		$res = $this->db->delete ( $this->device_table );
	}
	function valid_current_pass($id, $current_pass) {
		$cond = array (
				'id' => $id,
				'user_pass' => $current_pass 
		);
		$this->db->where ( $cond );
		$query = $this->db->get ( $this->user_table );
		if ($query->num_rows () === 1) {
			return $query->row ();
		} else {
			return false;
		}
	}
	
	// reset new password
	function updatepassword($table, $user_id, $new_pass) {
		$user_email = $this->db->select ( 'email_id' )->where ( 'care_id', $user_id )->limit ( 1 )->get ( 'users' )->result_array () [0] ['email_id'];
		$useremail = $user_email;
		$this->load->library ( 'email' );
		$this->email->from ( 'info@crxlife.com', 'CRX Life' );
		$this->email->to ( $useremail );
		$this->email->subject ( 'change Password' );
		$this->email->message ( 'Hi Your password has been changed: New Password is: ' . $new_pass );
		$this->email->send ();
		$post = $this->input->post ();
		$ins = array ();
		$ins ['password'] = md5 ( $new_pass );
		$this->db->where ( 'care_id', $user_id );
		$this->db->update ( $table, $ins );
	}
	public function validate_password($data) {
		try {
			$this->load->library ( 'form_validation' );
			$config = array (
					array (
							'field' => 'mobile',
							'label' => 'User Mobile',
							'rules' => 'trim|required' 
					) 
			);
			$this->form_validation->set_rules ( $config );
			if ($this->form_validation->run () == false) {
				$errors_array = '';
				foreach ( $config as $row ) {
					$field = $row ['field'];
					$error = strip_tags ( form_error ( $field ) );
					if ($error)
						$errors_array .= $error . ', ';
				}
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => rtrim ( $errors_array, ', ' ) 
				);
			} else {
				$pass = $this->generateStrongPassword ( '9' );
				$this->load->library ( 'email' );
				$this->email->from ( 'info@crxlife.com', 'CRX Life' );
				$this->email->to ( $data ['email_id'] );
				$this->email->subject ( 'Forgot Password' );
				$this->email->message ( 'Hi Your password has been changed: New Password is: ' . $pass );
				$this->email->send ();
				$user_email = $data ['email_id'];
				$user_mobile = $data ['mobile'];
				$updtate = array (
						'password' => md5 ( $pass ) 
				);
				$cond = array (
						'mobile' => $user_mobile 
				);
				$this->db->where ( $cond );
				$query = $this->db->update ( $this->user_table, $updtate );
				$message = array (
						'status' => true,
						'response_code' => '1',
						'password' => $pass,
						'message' => 'Password has been changed succeessfully.Please Check you mail.Thank You!' 
				);
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		return $message;
	}
	function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds') {
		$sets = array ();
		if (strpos ( $available_sets, 'l' ) !== false)
			$sets [] = 'abcdefghjkmnpqrstuvwxyz';
		if (strpos ( $available_sets, 'u' ) !== false)
			$sets [] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
		if (strpos ( $available_sets, 'd' ) !== false)
			$sets [] = '23456789';
		if (strpos ( $available_sets, 's' ) !== false)
			$sets [] = '!@#$%&*?';
		$all = '';
		$password = '';
		foreach ( $sets as $set ) {
			$password .= $set [array_rand ( str_split ( $set ) )];
			$all .= $set;
		}
		$all = str_split ( $all );
		for($i = 0; $i < $length - count ( $sets ); $i ++)
			$password .= $all [array_rand ( $all )];
		$password = str_shuffle ( $password );
		if (! $add_dashes)
			return $password;
		$dash_len = floor ( sqrt ( $length ) );
		$dash_str = '';
		while ( strlen ( $password ) > $dash_len ) {
			$dash_str .= substr ( $password, 0, $dash_len ) . '-';
			$password = substr ( $password, $dash_len );
		}
		$dash_str .= $password;
		return $dash_str;
	}
	public function updateprofile($id) {
		$file = $this->upload_profile_pic ();
		if ($file ['file_name'] != '') {
			$data = array (
					'photo_id' => $file ['file_name'] 
			);
			$this->db->where ( 'care_id', $id );
			$query = $this->db->update ( $this->user_table, $data );
			if ($query) {
				$filepath = base_url () . 'uploads/users/profile/' . $file ['file_name'];
				return $filepath;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	public function checkUser($data) {
		$userid = $data ['user_id'];
		$this->db->where ( 'id', $userid );
		$res = $this->db->get ( $this->user_table );
		if ($res->num_rows () > 0) {
			return true;
		} else {
			return false;
		}
	}
	public function CheckUserCredentials($data) {
		$cond = array (
				'user_mobile' => $data ['user_mobile'] 
		);
		$this->db->where ( $cond );
		$res = $this->db->get ( $this->user_table );
		if ($res->num_rows () > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	/* reg_id check android */
	function checkreg_id($table, $user_id) {
		$this->db->select ( 'android_device_id' );
		$this->db->where ( 'mobile', $user_id );
		$query = $this->db->get ( $table );
		return $query->row ();
	}
	
	/* Update reg_id for android */
	function updateregid($table, $mobile, $reg_id) {
		$data = array ();
		$data ['android_device_id'] = json_encode ( $reg_id );
		
		$this->db->where ( 'mobile', $mobile );
		$this->db->update ( $table, $data );
	}
	
	/* Logout For Device */
	function updaterlogout($table, $user_id, $reg_id) {
		$getuser = $this->db->select ( 'reg_id' )->where ( 'care_id', $user_id )->limit ( 1 )->get ( 'users' )->result_array () [0] ['reg_id'];
		$reg_user_id = json_decode ( $getuser );
		$key = array_search ( $reg_id, $reg_user_id ); // $key = 2;
		unset ( $reg_user_id [$key] );
		if (count ( $reg_user_id ) > 0) {
			$data = array ();
			foreach ( $reg_user_id as $k => $v ) {
				
				$reg_ids [] = $v;
				$data ['reg_id'] = json_encode ( $reg_ids );
			}
		} else {
			$data = array ();
			$data ['reg_id'] = '';
		}
		$this->db->where ( 'user_id', $user_id );
		$this->db->update ( $table, $data );
	}
	public function GetUpdateddata($id) {
		$this->db->where ( 'id', $id );
		$query = $this->db->get ( $this->user_table );
		return $query->row ();
	}
	function Deviceupdaterlogout($table, $careId, $deviceId) {
		$this->db->where ( array (
				'device_id' => $deviceId,
				'user_id' => $careId 
		) );
		$this->db->delete ( $table );
	}
	function androidnotification($careID, $action = 'default', $deviceID = '') {
		$this->db->select ( 'reg_id' );
		$this->db->where ( 'user_id', $careID );
		$this->db->where ( 'device_id', $deviceID );
		$this->db->where ( 'device_type', 'android' );
		$query = $this->db->get ( $this->device_table );
		$getuser = $query->result_array ();
		foreach ( $getuser as $val ) {
			$register_send = $val;
			$value = $register_send ['reg_id'];
			$get = json_decode ( $value );
			$notification = GetNonActionNotificationDetails ( $action );
			$message = $notification->message;
			$api_key = 'AIzaSyBxFRHbCn2sxHRgu2rdmDZn0Q-RIC46J8Y';
			
			if (empty ( $api_key ) || count ( $get ) < 0) {
				$result = array (
						'success' => '0',
						'message' => 'api or reg id not found' 
				);
				echo json_encode ( $result );
				die ();
			}
			$registrationIDs = $get;
			
			// $message = "congratulations";
			$url = 'https://android.googleapis.com/gcm/send';
			$resultData = array (
					'type' => 'activity',
					'title' => $notification->title 
			);
			$fields = array (
					'registration_ids' => $registrationIDs,
					'data' => array (
							"message" => $message,
							"title" => $notification->title,
							"type" => 'Signin',
							'data' => json_encode ( $resultData ) 
					) 
			);
			
			$headers = array (
					'Authorization: key=' . $api_key,
					'Content-Type: application/json' 
			);
			
			$ch = curl_init ();
			curl_setopt ( $ch, CURLOPT_URL, $url );
			curl_setopt ( $ch, CURLOPT_POST, true );
			curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
			curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
			$result = curl_exec ( $ch );
			
			curl_close ( $ch );
			// print_r($result);
		}
	}
	function iosNotification($careID, $action = 'default', $deviceID = '') {
		$this->db->select ( 'reg_id' );
		$this->db->where ( 'user_id', $careID );
		$this->db->where ( 'device_id', $deviceID );
		$this->db->where ( 'device_type', 'ios' );
		$query = $this->db->get ( $this->device_table );
		$getuser = $query->result_array ();
		$url = IMAGESPATH . 'ios-certified/LiveCert.pem';
		$CI = &get_instance ();
		$sound = 'default';
		$badge = '0';
		$content_aval = '1';
		$notification = GetNonActionNotificationDetails ( $action );
		$message = $notification->message;
		$message = $notification->message;
		$title = $notification->title;
		print_r ( $getuser );
		die ();
		foreach ( $getuser as $valios ) {
			// print_r($valios);
			$register_send = $valios;
			$value = $register_send ['reg_id'];
			$gets = json_decode ( $value );
			foreach ( $gets as $get ) {
				$dtoken = $get;
				$message = $message;
				// My device token here (without spaces):
				$deviceToken = $dtoken;
				// print_r($deviceToken); die;
				// My private key's passphrase here:
				$passphrase = 'BCG4pp$';
				
				// My alert message here:
				// $message = 'New Push Notification!';
				// badge
				$badge = 0;
				
				$ctx = stream_context_create ();
				stream_context_set_option ( $ctx, 'ssl', 'local_cert', $url );
				stream_context_set_option ( $ctx, 'ssl', 'passphrase', $passphrase );
				
				// Open a connection to the APNS server
				$fp = stream_socket_client ( 'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx );
				
				if (! $fp)
					exit ( "Failed to connect: $err $errstr" . PHP_EOL );
					
					// echo 'Connected to APNS' . PHP_EOL;
					// Create the payload body
				$body ['aps'] = array (
						'alert' => $message,
						'badge' => $badge,
						'sound' => 'default' 
				);
				
				// Encode the payload as JSON
				$payload = json_encode ( $body );
				
				// Build the binary notification
				$msg = chr ( 0 ) . pack ( 'n', 32 ) . pack ( 'H*', $deviceToken ) . pack ( 'n', strlen ( $payload ) ) . $payload;
				
				// Send it to the server
				$result = fwrite ( $fp, $msg, strlen ( $msg ) );
				
				/*
				 * if (!$result)
				 * echo 'Error, notification not sent' . PHP_EOL;
				 * else
				 * echo 'notification sent!' . PHP_EOL;
				 */
				
				// Close the connection to the server
				
				fclose ( $fp );
			}
			// print_r($result);
			// die;
		}
	}
	public function getParentID($appid) {
		$this->db->where ( 'app_id', $appid );
		$query = $this->db->get ( $this->user_table );
		
		// echo $this->db->last_query(); die();
		return $query->row ();
	}
	public function fetchcategoryRecord($id) {
		$response = array ();
		$this->db->where ( 'category.app_id', $id );
		
		$query = $this->db->get ( 'category' );
		// echo $this->db->last_query(); die();
		$result = $query->result_array ();
		
		foreach ( $result as $row ) {
			$catid = $row ['category_id'];
			$row ['subcategories'] = $this->fetchsubcategoryByCatID ( $catid, $id );
			array_push ( $response, $row );
		}
		
		return $response;
	}
	public function fetchcategoryByID($cat_id) {
		$this->db->where ( 'category.category_id', $cat_id );
		
		$query = $this->db->get ( 'category' );
		// echo $this->db->last_query(); die();
		return $query->result ();
	}
	public function fetchsubcategoryByID($cat_id) {
		$this->db->where ( 'subcategory.sub_cat_id', $cat_id );
		
		$query = $this->db->get ( 'subcategory' );
		// echo $this->db->last_query(); die();
		return $query->result_array ();
	}
	public function fetchsubcategoryByCatID($cat_id, $id) {
		$this->db->where ( 'subcategory.main_cat_id', $cat_id );
		$this->db->where ( 'subcategory.app_id', $id );
		$this->db->select ( 'subcategory.*,if(user_pic="","" ,CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",user_pic ) ) user_pic' );
		$this->db->from ( 'subcategory' );
		$query = $this->db->get ();
		$response = array ();
		$result = $query->result ();
		foreach ( $result as $col ) {
			$sub_cat_id = $col->sub_cat_id;
			$col->galleryImage = $this->GetImageGallery ( $sub_cat_id, $id );
			array_push ( $response, $col );
		}
		
		return $response;
	}
	public function GetImageGallery($sub_cat_id, $id) {
		$this->db->where ( 'nc_gallery.sub_cat_id', $sub_cat_id );
		$this->db->where ( 'nc_gallery.app_id', $id );
		$this->db->select ( 'if(image_name="","" ,CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",image_name ) ) image_name' );
		$this->db->from ( 'nc_gallery' );
		$query = $this->db->get ();
		
		// echo $this->db->last_query(); die();
		return $query->result_array ();
	}
	public function getPrivacyPolicy($appID) {
		$this->db->where ( 'nc_privacy.app_id', $appID );
		$query = $this->db->get ( 'nc_privacy' );
		return $query->result ();
	}
	public function getversionList($appID) {
		$this->db->where ( 'app_id', $appID );
		$this->db->select ( 'version_number' );
		$this->db->from ( $this->version );
		$res = $this->db->get ();
		return $res->row ();
	}
	public function GetUserInformation($userid) {
		$this->db->where ( 'id', $userid );
		$this->db->select ( '*,if(user_pic="","" ,CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",user_pic ) ) user_pic' );
		$this->db->from ( $this->user_table );
		$result = $this->db->get ();
		// echo $this->db->last_query(); die();
		return $result->row ();
	}
	public function ask_query($data) {
		$this->db->set ( 'created_date', 'NOW()', false );
		$this->db->insert ( $this->query, $data );
		$insert_id = $this->db->insert_id ();
		
		if ($insert_id) {
			$message = array (
					'status' => true,
					'response_code' => '1',
					'message' => "Your query has been submitted Successfully",
					'query_id' => $insert_id 
			);
		}
		
		return $message;
	}
	public function GetQueriesByAppID($appid, $userid) {
		$this->db->where ( 'app_id', $appid );
		$this->db->where ( 'user_id', $userid );
		$query = $this->db->get ( 'query' );
		// echo $this->db->last_query(); die();
		return $query->result ();
	}
	public function GetAppDesc($appid) {
		$this->db->where ( 'app_id', $appid );
		$this->db->select ( 'app_name' );
		$this->db->from ( 'application' );
		$res = $this->db->get ();
		// echo $this->db->last_query(); die();
		return $res->row ();
	}
}

?>