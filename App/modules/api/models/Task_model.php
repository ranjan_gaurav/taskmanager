<?php

/**
 * Created by PhpStorm.
 * User: Gaurav Ranjan<ranjan.gaurav@orangemantra.in>
 * Date: 15/01/2016
 * Time: 11:28 AM
 */
class Task_model extends CI_Model {
	var $task_table = "tasks";
	// var $device_table = "device_info";
	function __construct() {
		parent::__construct ();
	}
	
	/**
	 * ription : Get User data
	 *
	 * @param null $ID        	
	 * @return mixed data
	 *         @date : 04/12/2015
	 */
	public function getAllUsers($ID = null) {
		if ($ID != null) {
			$this->db->where ( 'id', $ID );
		}
		$this->db->where ( 'status', '1' );
		$this->db->select ( '*,if(user_pic="","null" ,CONCAT("' . base_url ( 'uploads/users/profile/' ) . '/",user_pic ) ) user_pic' );
		$this->db->from ( $this->user_table );
		$result = $this->db->get ();
		return $result->result ();
	}
	public function get_details_result($user_id) {
		$this->db->where ( 'care_id', $user_id );
		$this->db->select ( 'id,user_pic' );
		$this->db->from ( $this->user_table );
		$result = $this->db->get ();
		return $result->result ();
	}
	
	/**
	 * ription : Add new User in the User table
	 *
	 * @method : Add User
	 * @param
	 *        	$data
	 * @return bool @date : 04/12/2015
	 */
	public function addTask($data) {
		try {
			
			$this->load->library ( 'form_validation' );
			$config = array (
					array (
							'field' => 'task_title',
							'label' => 'task_title',
							'rules' => 'trim|required' 
					) 
			);
			
			$this->form_validation->set_rules ( $config );
			// $this->form_validation->set_message ( 'is_unique', 'You are already a registered member. Please Sign-in or click on forgot password.' );
			if ($this->form_validation->run () == false) {
				$errors_array = '';
				foreach ( $config as $row ) {
					$field = $row ['field'];
					$error = strip_tags ( form_error ( $field ) );
					if ($error)
						$errors_array .= $error . ', ';
				}
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => rtrim ( $errors_array, ', ' ) 
				);
			} else {
				$this->db->set ( 'created_date', 'NOW()', false );
				$this->db->set ( 'modified_on', 'NOW()', false );
				$this->db->insert ( $this->task_table, $data );
				$insert_id = $this->db->insert_id ();
				
				if ($insert_id) {
					$insert ['task_id'] = $insert_id;
					$insert ['assigned_by'] = $this->input->post ( 'assigned_by' );
					$insert ['assigned_to'] = $this->input->post ( 'assigned_to' );
					// $insert['assigned_on'] = $data [];
					$this->db->set ( 'assigned_on', date ( 'Y-m-d H:i:s' ) );
					if ($data ['assign_type'] == 1) {
						$result = $this->db->insert ( 'assigned_tasks_team', $insert );
					} else {
						$result = $this->db->insert ( 'assigned_tasks_group', $insert );
					}
				}
				
				$message = array (
						'status' => true,
						'response_code' => '1',
						'message' => "Task Added Successfully",
						'taskid' => $insert_id 
				);
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		return $message;
	}
	public function assignTask($data) {
		$task_id = json_decode ( $data ['task_id'] ); // print_r($leadids); die();
		
		$update ['is_assigned'] = '1';
		$update ['assign_type'] = $data ['assign_type'];
		
		// $update ['assigned_to'] = $data ['receiver_id'];
		
		// $this->db->set ( 'assigned_on', date ( 'Y-m-d H:i:s', now () ) );
		$this->db->set ( 'modified_on', date ( 'Y-m-d H:i:s' ) );
		$this->db->where_in ( 'task_id', $task_id );
		$result = $this->db->update ( 'tasks', $update );
		
		if ($data ['assign_type'] == 1) {
			foreach ( $task_id as $ids ) {
				
				$this->db->where ( 'task_id', $ids );
				$result = $this->db->get ( 'assigned_tasks_team' );
				
				if ($result->num_rows () == 0) {
					$insert ['assigned_by'] = $data ['sender_id'];
					$insert ['task_id'] = $ids;
					$insert ['assigned_to'] = $data ['receiver_id'];
					
					$this->db->set ( 'assigned_on', date ( 'Y-m-d H:i:s' ) );
					$res = $this->db->insert ( 'assigned_tasks_team', $insert );
				} 

				else {
					$insert ['assigned_by'] = $data ['sender_id'];
					$insert ['task_id'] = $ids;
					$insert ['assigned_to'] = $data ['receiver_id'];
					
					$this->db->where ( 'task_id', $ids );
					$this->db->set ( 'assigned_on', date ( 'Y-m-d H:i:s' ) );
					$res = $this->db->update ( 'assigned_tasks_team', $insert );
				}
			}
			
			$msg = "Task sucessfully assigned to" . ' ' . $data ['receiver_name'];
		}
		
		if ($data ['assign_type'] == 2) {
			foreach ( $task_id as $ids ) {
				
				$this->db->where ( 'task_id', $ids );
				$result = $this->db->get ( 'assigned_tasks_group' );
				
				if ($result->num_rows () == 0) {
					$insert ['assigned_by'] = $data ['sender_id'];
					$insert ['task_id'] = $ids;
					$insert ['assigned_to'] = $data ['receiver_id'];
					
					$this->db->set ( 'assigned_on', date ( 'Y-m-d H:i:s', now () ) );
					$res = $this->db->insert ( 'assigned_tasks_group', $insert );
				} 

				else {
					$insert ['assigned_by'] = $data ['sender_id'];
					$insert ['task_id'] = $ids;
					$insert ['assigned_to'] = $data ['receiver_id'];
					
					$this->db->where ( 'task_id', $ids );
					$this->db->set ( 'assigned_on', date ( 'Y-m-d H:i:s', now () ) );
					$res = $this->db->update ( 'assigned_tasks_group', $insert );
				}
			}
			
			$msg = "Task sucessfully assigned to" . ' ' . $data ['receiver_name'];
		}
		
		/*
		 * if ($res) {
		 * $GetDevice = $this->deviceType ( $data ['receiver_id'] ); //print_r($GetDevice); die();
		 * // $msg = $Sendername . " has created new group";
		 * // $Type = "LeadsAssign";
		 *
		 * foreach ( $GetDevice as $device ) {
		 *
		 *
		 * $info ['message'] = $data ['sender_name'] . " has assigned one lead to you";
		 * $info ['Type'] = "LeadsAssignMent";
		 * $info ['Title'] = "LeadsAssignMent";
		 * $info ['receiver_id'] = $data ['receiver_id'];
		 * $info ['sender_id'] = $data ['sender_id'];
		 *
		 * if ($device ['deviceType'] == 'android') {
		 * $info ['devices'] = $device ['fcmId'];
		 * Android_notification ($info);
		 * } else {
		 *
		 * $info ['devices'] = $device ['reg_id_ios'];
		 * Ios_notification ($info);
		 * }
		 *
		 *
		 *
		 * }
		 * }
		 */
		if ($result) {
			$message = array (
					'status' => true,
					'response_code' => '1',
					'message' => $msg 
			);
			// 'taskid' => $insert_id
			
			// $response->setObjArray ( $res->result () );
		} else {
			$message = array (
					'status' => true,
					'response_code' => '1',
					'message' => "There is some problem in assigning data" 
			);
			// 'taskid' => $insert_id
		}
		
		return $message;
	}
	public function changeStatus($data) {
		$status = $data ['statusId'];
		$taskid = $data ['task_id'];
		
		$query = "UPDATE tasks SET status= $status WHERE task_id IN ($taskid)"; // print_r($query); die();
		$result = $this->db->query ( $query );
		
		if ($result) {
			
			$query = "INSERT INTO status_log (`task_id`, `status`, `note`,`changed_by`,`created_date`)
					  VALUES (" . $this->db->escape ( $data ['task_id'] ) . "," . $this->db->escape ( $data ['statusId'] ) . "," . $this->db->escape ( $data ['note'] ) . "," . $this->db->escape ( $data ['changed_by'] ) . ",now())";
			$result = $this->db->query ( $query );
			
			if ($result) {
				$message = array (
						'status' => true,
						'response_code' => '1',
						'message' => "Status successfully changed" 
				);
			} else {
				
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => "There is some problem in updating status......" 
				);
				
				// $response->setObjArray($result->result());
			}
		}
		
		return $message;
	}
	public function GetrecentTasks($userid) {
		$GetRole = $this->GetUserRole ( $userid );
		// $date;
		
		if ($GetRole->role_id == 1) {
			// $this->db->limit ( 3 );
			$this->db->where ( "DATE(tasks.created_date) BETWEEN '" . date ( 'Y-m-d', strtotime ( '-3 days' ) ) . "' AND '" . date ( 'Y-m-d' ) . "'" );
			$this->db->select ( "tasks.*,concat(u2.firstname,' ',u2.lastname) as added_by_name,(CASE WHEN tasks.assign_type = 1 THEN team.team_title END) as team_title ,(CASE WHEN tasks.assign_type = 1 THEN 'Individual' WHEN tasks.assign_type = 2 THEN 'Group' END) as assign_type ,
					(CASE WHEN tasks.assign_type = 1 THEN concat(u1.firstname ,' ', u1.lastname) WHEN tasks.assign_type = 2 THEN groups.group_name END) as assignToName,(CASE WHEN tasks.assign_type = 1 THEN assigned_tasks_team.assigned_to WHEN tasks.assign_type = 2 THEN assigned_tasks_group.assigned_to END) as assigned_to,status.name as status" );
			$this->db->from ( 'tasks' );
			$this->db->join ( 'assigned_tasks_team', 'tasks.task_id = assigned_tasks_team.task_id and assign_type = 1', 'left' );
			$this->db->join ( 'assigned_tasks_group', 'tasks.task_id = assigned_tasks_group.task_id and assign_type = 2', 'left' );
			$this->db->join ( 'users u1', 'assigned_tasks_team.assigned_to = u1.user_id or assigned_tasks_group.assigned_to = u1.user_id', 'left' );
			$this->db->join ( 'users u2', 'tasks.added_by = u2.user_id', 'left' );
			$this->db->join ( 'team_member', 'team_member.user_id = u1.user_id', 'left' );
			$this->db->join ( 'team', 'team_member.team_id = team.team_id', 'left' );
			$this->db->join ( 'status', 'tasks.status = status.id', 'left' );
			$this->db->join ( 'groups', 'assigned_tasks_group.assigned_to = groups.group_id', 'left' );
			$this->db->order_by ( 'tasks.created_date', 'DESC' );
			$res = $this->db->get (); // echo $this->db->last_query(); die();
		}
		
		if ($GetRole->role_id == 2) {
			// $this->db->limit ( 3 );
			$this->db->where ( "DATE(tasks.created_date) BETWEEN '" . date ( 'Y-m-d', strtotime ( '-3 days' ) ) . "' AND '" . date ( 'Y-m-d' ) . "'" );
			$this->db->where ( 'assigned_tasks_team.assigned_by', $userid );
			$this->db->or_where ( 'assigned_tasks_team.assigned_to', $userid );
			// $this->db->or_where ( 'assigned_tasks_group.assigned_by', $userid );
			// $this->db->or_where ( 'assigned_tasks_group.assigned_to', $userid );
			$this->db->or_where ( 'group_users.user_id_fk', $userid );
			$this->db->select ( "tasks.*,concat(u2.firstname,' ',u2.lastname) as added_by_name,team.team_title ,(CASE WHEN tasks.assign_type = 1 THEN 'Individual' WHEN tasks.assign_type = 2 THEN 'Group' END) as assign_type ,(CASE WHEN tasks.assign_type = 1 THEN concat(users.firstname ,' ', users.lastname) WHEN tasks.assign_type = 2 THEN groups.group_name END) as assignToName,(CASE WHEN tasks.assign_type = 1 THEN assigned_tasks_team.assigned_to WHEN tasks.assign_type = 2 THEN assigned_tasks_group.assigned_to END) as assigned_to,status.name as status" );
			$this->db->from ( 'tasks' );
			$this->db->join ( 'assigned_tasks_team', 'tasks.task_id = assigned_tasks_team.task_id and assign_type = 1', 'left' );
			$this->db->join ( 'assigned_tasks_group', 'tasks.task_id = assigned_tasks_group.task_id and assign_type = 2', 'left' );
			$this->db->join ( 'group_users', 'group_users.group_id_fk = assigned_tasks_group.assigned_to', 'left' );
			$this->db->join ( 'users', 'assigned_tasks_team.assigned_to = users.user_id or assigned_tasks_group.assigned_to = users.user_id', 'left' );
			$this->db->join ( 'users u2', 'tasks.added_by = u2.user_id', 'left' );
			$this->db->join ( 'team_member', 'team_member.user_id = users.user_id', 'left' );
			$this->db->join ( 'team', 'team_member.team_id = team.team_id', 'left' );
			$this->db->join ( 'status', 'tasks.status = status.id', 'left' );
			$this->db->join ( 'groups', 'assigned_tasks_group.assigned_to = groups.group_id', 'left' );
			$this->db->order_by ( 'tasks.created_date', 'DESC' );
			$res = $this->db->get (); // echo $this->db->last_query();
		}
		
		if ($GetRole->role_id == 3) {
			$this->db->where ( "DATE(tasks.created_date) BETWEEN '" . date ( 'Y-m-d', strtotime ( '-3 days' ) ) . "' AND '" . date ( 'Y-m-d' ) . "'" );
			$this->db->where ( 'assigned_tasks_team.assigned_to', $userid );
			$this->db->or_where ( 'group_users.user_id_fk', $userid );
			$this->db->select ( "tasks.*,concat(u2.firstname,' ',u2.lastname) as added_by_name,(CASE WHEN tasks.assign_type = 1 THEN team.team_title END) as team_title ,(CASE WHEN tasks.assign_type = 1 THEN 'Individual' WHEN tasks.assign_type = 2 THEN 'Group' END) as assign_type ,(CASE WHEN tasks.assign_type = 1 THEN concat(users.firstname ,' ', users.lastname) WHEN tasks.assign_type = 2 THEN groups.group_name END) as assignToName,(CASE WHEN tasks.assign_type = 1 THEN assigned_tasks_team.assigned_to WHEN tasks.assign_type = 2 THEN assigned_tasks_group.assigned_to END) as assigned_to,status.name as status" );
			$this->db->from ( 'tasks' );
			$this->db->join ( 'assigned_tasks_team', 'tasks.task_id = assigned_tasks_team.task_id and assign_type = 1', 'left' );
			$this->db->join ( 'assigned_tasks_group', 'tasks.task_id = assigned_tasks_group.task_id and assign_type = 2', 'left' );
			$this->db->join ( 'group_users', 'group_users.group_id_fk = assigned_tasks_group.assigned_to', 'left' );
			$this->db->join ( 'users', 'assigned_tasks_team.assigned_to = users.user_id or assigned_tasks_group.assigned_to = users.user_id', 'left' );
			$this->db->join ( 'users u2', 'tasks.added_by = u2.user_id', 'left' );
			$this->db->join ( 'team_member', 'team_member.user_id = users.user_id', 'left' );
			$this->db->join ( 'team', 'team_member.team_id = team.team_id', 'left' );
			$this->db->join ( 'status', 'tasks.status = status.id', 'left' );
			$this->db->join ( 'groups', 'assigned_tasks_group.assigned_to = groups.group_id', 'left' );
			$this->db->order_by ( 'tasks.created_date', 'DESC' );
			$res = $this->db->get (); // echo $this->db->last_query(); die();
		}
		
		return $res->result ();
	}
	public function GetAllTasks($userid) {
		$GetRole = $this->GetUserRole ( $userid );
		
		if ($GetRole->role_id == 1) {
			// $this->db->limit(3);
			$this->db->select ( "tasks.*,concat(u2.firstname,' ',u2.lastname) as added_by_name,
					          (CASE WHEN tasks.assign_type = 1 THEN team.team_title END) as team_title ,
					          (CASE WHEN tasks.assign_type = 1 THEN team.team_id END) as team_id,
					          (CASE WHEN tasks.assign_type = 1 THEN 'Individual' WHEN tasks.assign_type = 2 THEN 'Group' END) as assign_type ,
					          (CASE WHEN tasks.assign_type = 1 THEN concat(users.firstname ,' ', users.lastname) WHEN tasks.assign_type = 2 THEN groups.group_name END) as assignToName,
					          (CASE WHEN tasks.assign_type = 1 THEN assigned_tasks_team.assigned_to WHEN tasks.assign_type = 2 THEN assigned_tasks_group.assigned_to END) as assigned_to,
					          status.name as status" );
			$this->db->from ( 'tasks' );
			$this->db->join ( 'assigned_tasks_team', 'tasks.task_id = assigned_tasks_team.task_id and assign_type = 1', 'left' );
			$this->db->join ( 'assigned_tasks_group', 'tasks.task_id = assigned_tasks_group.task_id and assign_type = 2', 'left' );
			$this->db->join ( 'users', 'assigned_tasks_team.assigned_to = users.user_id or assigned_tasks_group.assigned_to = users.user_id', 'left' );
			$this->db->join ( 'users u2', 'tasks.added_by = u2.user_id', 'left' );
			$this->db->join ( 'team_member', 'team_member.user_id = users.user_id', 'left' );
			$this->db->join ( 'team', 'team_member.team_id = team.team_id', 'left' );
			$this->db->join ( 'status', 'tasks.status = status.id', 'left' );
			$this->db->join ( 'groups', 'assigned_tasks_group.assigned_to = groups.group_id', 'left' );
			$this->db->order_by ( 'tasks.created_date', 'DESC' );
			$res = $this->db->get (); // echo $this->db->last_query();
		}
		
		if ($GetRole->role_id == 2) {
			// $this->db->limit(3);
			$this->db->where ( 'assigned_tasks_team.assigned_by', $userid );
			$this->db->or_where ( 'assigned_tasks_team.assigned_to', $userid );
			// $this->db->or_where ( 'assigned_tasks_group.assigned_by', $userid );
			// $this->db->or_where ( 'assigned_tasks_group.assigned_to', $userid );
			$this->db->or_where ( 'group_users.user_id_fk', $userid );
			$this->db->select ( "tasks.*,concat(u2.firstname,' ',u2.lastname) as added_by_name,
					          (CASE WHEN tasks.assign_type = 1 THEN team.team_title END) as team_title ,
					          (CASE WHEN tasks.assign_type = 1 THEN team.team_id END) as team_id,
					          (CASE WHEN tasks.assign_type = 1 THEN 'Individual' WHEN tasks.assign_type = 2 THEN 'Group' END) as assign_type ,
					          (CASE WHEN tasks.assign_type = 1 THEN concat(users.firstname ,' ', users.lastname) WHEN tasks.assign_type = 2 THEN groups.group_name END) as assignToName,
					          (CASE WHEN tasks.assign_type = 1 THEN assigned_tasks_team.assigned_to WHEN tasks.assign_type = 2 THEN assigned_tasks_group.assigned_to END) as assigned_to,
					          status.name as status" );
			$this->db->from ( 'tasks' );
			$this->db->join ( 'assigned_tasks_team', 'tasks.task_id = assigned_tasks_team.task_id and assign_type = 1', 'left' );
			$this->db->join ( 'assigned_tasks_group', 'tasks.task_id = assigned_tasks_group.task_id and assign_type = 2', 'left' );
			$this->db->join ( 'group_users', 'group_users.group_id_fk = assigned_tasks_group.assigned_to', 'left' );
			$this->db->join ( 'users', 'assigned_tasks_team.assigned_to = users.user_id or assigned_tasks_group.assigned_to = users.user_id', 'left' );
			$this->db->join ( 'users u2', 'tasks.added_by = u2.user_id', 'left' );
			$this->db->join ( 'team_member', 'team_member.user_id = users.user_id', 'left' );
			$this->db->join ( 'team', 'team_member.team_id = team.team_id', 'left' );
			$this->db->join ( 'status', 'tasks.status = status.id', 'left' );
			$this->db->join ( 'groups', 'assigned_tasks_group.assigned_to = groups.group_id', 'left' );
			$this->db->order_by ( 'tasks.created_date', 'DESC' );
			$res = $this->db->get (); // echo $this->db->last_query();
		}
		
		if ($GetRole->role_id == 3) {
			// $this->db->limit(3);
			$this->db->where ( 'assigned_tasks_team.assigned_to', $userid );
			$this->db->or_where ( 'group_users.user_id_fk', $userid );
			$this->db->select ( "tasks.*,concat(u2.firstname,' ',u2.lastname) as added_by_name,
					   (CASE WHEN tasks.assign_type = 1 THEN team.team_id END) as team_title ,
					   (CASE WHEN tasks.assign_type = 1 THEN team.team_id END) as team_id,
					   (CASE WHEN tasks.assign_type = 1 THEN 'Individual' WHEN tasks.assign_type = 2 THEN 'Group' END) as assign_type ,
					   (CASE WHEN tasks.assign_type = 1 THEN concat(users.firstname ,' ', users.lastname) WHEN tasks.assign_type = 2 THEN groups.group_name END) as assignToName,
					   (CASE WHEN tasks.assign_type = 1 THEN assigned_tasks_team.assigned_to WHEN tasks.assign_type = 2 THEN assigned_tasks_group.assigned_to END) as assigned_to,
					   status.name as status" );
			$this->db->from ( 'tasks' );
			$this->db->join ( 'assigned_tasks_team', 'tasks.task_id = assigned_tasks_team.task_id and assign_type = 1', 'left' );
			$this->db->join ( 'assigned_tasks_group', 'tasks.task_id = assigned_tasks_group.task_id and assign_type = 2', 'left' );
			$this->db->join ( 'group_users', 'group_users.group_id_fk = assigned_tasks_group.assigned_to', 'left' );
			$this->db->join ( 'users', 'assigned_tasks_team.assigned_to = users.user_id or assigned_tasks_group.assigned_to = users.user_id', 'left' );
			$this->db->join ( 'users u2', 'tasks.added_by = u2.user_id', 'left' );
			$this->db->join ( 'team_member', 'team_member.user_id = users.user_id', 'left' );
			$this->db->join ( 'team', 'team_member.team_id = team.team_id', 'left' );
			$this->db->join ( 'status', 'tasks.status = status.id', 'left' );
			$this->db->join ( 'groups', 'assigned_tasks_group.assigned_to = groups.group_id', 'left' );
			$this->db->order_by ( 'tasks.created_date', 'DESC' );
			$res = $this->db->get (); // echo $this->db->last_query();
		}
		
		return $res->result ();
	}
	public function GetUserRole($data) {
		$query = "select role_id,concat(firstname,'',lastname) as name from users where user_id = $data";
		$result = $this->db->query ( $query );
		return $result->row ();
	}
	
	/**
	 * ription : To Upload Profile Pic
	 *
	 * @param
	 *        	$id
	 * @return bool @date : 19/01/2016
	 * @method : Upload Profile Pic
	 */
	protected function upload_profile_pic() {
		$config = array ();
		ini_set ( 'upload_max_filesize', '200M' );
		ini_set ( 'post_max_size', '200M' );
		ini_set ( 'max_input_time', 3000 );
		ini_set ( 'max_execution_time', 3000 );
		$config ['upload_path'] = IMAGESPATH . 'users/profile/';
		$config ['allowed_types'] = '*'; // 'mp3|wav';
		$config ['file_name'] = md5 ( uniqid ( rand (), true ) );
		$this->load->library ( 'upload', $config );
		$this->upload->initialize ( $config );
		if ($this->upload->do_upload ( 'photo_id' )) {
			$info = $this->upload->data ();
			return $info;
		}
	}
	public function getTaskByTeam($userid, $teamID) {
		$GetRole = $this->GetUserRole ( $userid, $teamID );
		if ($GetRole->role_id == 1) {
			
			$this->db->where ( 'team.team_id', $teamID );
			$this->db->where ( 'tasks.assign_type', 1 );
			$this->db->select ( "tasks.*,concat(u2.firstname,' ',u2.lastname) as added_by_name,
					(CASE WHEN tasks.assign_type = 1 THEN team.team_title END) as team_title ,
					(CASE WHEN tasks.assign_type = 1 THEN team.team_id END) as team_id,
					(CASE WHEN tasks.assign_type = 1 THEN 'Individual' WHEN tasks.assign_type = 2 THEN 'Group' END) as assign_type ,
					(CASE WHEN tasks.assign_type = 1 THEN concat(users.firstname ,' ', users.lastname) WHEN tasks.assign_type = 2 THEN groups.group_name END) as assignToName,
					(CASE WHEN tasks.assign_type = 1 THEN assigned_tasks_team.assigned_to WHEN tasks.assign_type = 2 THEN assigned_tasks_group.assigned_to END) as assigned_to,
					status.name as status" );
			$this->db->from ( 'tasks' );
			$this->db->join ( 'assigned_tasks_team', 'tasks.task_id = assigned_tasks_team.task_id and assign_type = 1', 'left' );
			$this->db->join ( 'assigned_tasks_group', 'tasks.task_id = assigned_tasks_group.task_id and assign_type = 2', 'left' );
			$this->db->join ( 'users', 'assigned_tasks_team.assigned_to = users.user_id or assigned_tasks_group.assigned_to = users.user_id', 'left' );
			$this->db->join ( 'users u2', 'tasks.added_by = u2.user_id', 'left' );
			$this->db->join ( 'team_member', 'team_member.user_id = users.user_id', 'left' );
			$this->db->join ( 'team', 'team_member.team_id = team.team_id', 'left' );
			$this->db->join ( 'status', 'tasks.status = status.id', 'left' );
			$this->db->join ( 'groups', 'assigned_tasks_group.assigned_to = groups.group_id', 'left' );
			$this->db->order_by ( 'tasks.created_date', 'DESC' );
			$res = $this->db->get (); // echo $this->db->last_query(); die();
			
			return $res->result ();
		} 

		else if ($GetRole->role_id == 2) {
			
			$this->db->where ( 'team.team_id', $teamID );
			$this->db->where ( 'tasks.assign_type', 1 );
			$this->db->where ( 'tasks.added_by', $userid );
			$this->db->select ( "tasks.*,concat(u2.firstname,' ',u2.lastname) as added_by_name,
					(CASE WHEN tasks.assign_type = 1 THEN team.team_title END) as team_title ,
					(CASE WHEN tasks.assign_type = 1 THEN team.team_id END) as team_id,
					(CASE WHEN tasks.assign_type = 1 THEN 'Individual' WHEN tasks.assign_type = 2 THEN 'Group' END) as assign_type ,
					(CASE WHEN tasks.assign_type = 1 THEN concat(users.firstname ,' ', users.lastname) WHEN tasks.assign_type = 2 THEN groups.group_name END) as assignToName,
					(CASE WHEN tasks.assign_type = 1 THEN assigned_tasks_team.assigned_to WHEN tasks.assign_type = 2 THEN assigned_tasks_group.assigned_to END) as assigned_to,
					status.name as status" );
			$this->db->from ( 'tasks' );
			$this->db->join ( 'assigned_tasks_team', 'tasks.task_id = assigned_tasks_team.task_id and assign_type = 1', 'left' );
			$this->db->join ( 'assigned_tasks_group', 'tasks.task_id = assigned_tasks_group.task_id and assign_type = 2', 'left' );
			$this->db->join ( 'users', 'assigned_tasks_team.assigned_to = users.user_id or assigned_tasks_group.assigned_to = users.user_id', 'left' );
			$this->db->join ( 'users u2', 'tasks.added_by = u2.user_id', 'left' );
			$this->db->join ( 'team_member', 'team_member.user_id = users.user_id', 'left' );
			$this->db->join ( 'team', 'team_member.team_id = team.team_id', 'left' );
			$this->db->join ( 'status', 'tasks.status = status.id', 'left' );
			$this->db->join ( 'groups', 'assigned_tasks_group.assigned_to = groups.group_id', 'left' );
			$this->db->order_by ( 'tasks.created_date', 'DESC' );
			$res = $this->db->get (); // echo $this->db->last_query(); die();
			
			return $res->result ();
		} 

		else {
			return "You are not authorised to view this.";
		}
	}
	public function GettaskByTeamWise($teamID) {
	}
	public function getTaskByGroup($userid) {
		$GetRole = $this->GetUserRole ( $userid );
		if ($GetRole == 1 || $GetRole == 2) {
			
			$this->db->select ( 'groups.*,IF(groups.photo_name="" ,"null", CONCAT("' . base_url ( 'uploads/gallery/' ) . '/",groups.photo_name )) photo_name,concat(users.firstname ," ", users.lastname) as groupAdminname,COUNT(user_id_fk) as users_count' );
			$this->db->from ( 'group_users' );
			$this->db->join ( 'groups', 'groups.group_id =  group_users.group_id_fk' );
			$this->db->join ( 'users', 'groups.group_admin =  users.user_id' );
			$this->db->group_by ( 'group_id_fk' );
			$res = $this->db->get ();
			
			$array = array ();
			
			foreach ( $res->result_array () as $row ) {
				$row ['tasks'] = $this->GettaskByGroupWise ( $row ['group_id'] );
				array_push ( $array, $row );
			}
			
			return $array;
		}
	}
	public function GettaskByGroupWise($group_id) {
		$this->db->where ( 'groups.group_id', $group_id );
		$this->db->where ( 'tasks.assign_type', 2 );
		$this->db->select ( "tasks.*,(CASE WHEN tasks.assign_type = 1 THEN team.team_title END) as team_title ,(CASE WHEN tasks.assign_type = 1 THEN 'Individual' WHEN tasks.assign_type = 2 THEN 'Group' END) as assign_type ,
					(CASE WHEN tasks.assign_type = 1 THEN concat(users.firstname ,' ', users.lastname) WHEN tasks.assign_type = 2 THEN groups.group_name END) as assignToName,status.name as status" );
		$this->db->from ( 'tasks' );
		$this->db->join ( 'assigned_tasks_team', 'tasks.task_id = assigned_tasks_team.task_id and assign_type = 1', 'left' );
		$this->db->join ( 'assigned_tasks_group', 'tasks.task_id = assigned_tasks_group.task_id and assign_type = 2', 'left' );
		$this->db->join ( 'users', 'assigned_tasks_team.assigned_to = users.user_id or assigned_tasks_group.assigned_to = users.user_id', 'left' );
		$this->db->join ( 'team_member', 'team_member.user_id = users.user_id', 'left' );
		$this->db->join ( 'team', 'team_member.team_id = team.team_id', 'left' );
		$this->db->join ( 'status', 'tasks.status = status.id', 'left' );
		$this->db->join ( 'groups', 'assigned_tasks_group.assigned_to = groups.group_id', 'left' );
		$this->db->order_by ( 'tasks.created_date', 'DESC' );
		$res = $this->db->get (); // secho $this->db->last_query(); die();
		
		return $res->result ();
	}
}

?>