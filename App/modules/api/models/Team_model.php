<?php

/**
 * Created by PhpStorm.
 * User: Gaurav Ranjan<ranjan.gaurav@orangemantra.in>
 * Date: 15/01/2016
 * Time: 11:28 AM
 */
class Team_model extends CI_Model {
	var $user_table = "users";
	var $device_table = "device_info";
	
	function __construct() {
		parent::__construct ();
	}
	
	/**
	 * ription : Get User data
	 *
	 * @param null $ID        	
	 * @return mixed data
	 *         @date : 04/12/2015
	 */
	public function getAllTeams() {
		
		//$this->db->where ( 'status', '1' );
		$this->db->select ("team.*,concat(users.firstname ,' ', users.lastname) as teamleadName");
		$this->db->from ('team');
		$this->db->join('users','team.team_lead = users.user_id');
		$this->db->order_by('team_title','ASC');
		$result = $this->db->get ();
		//return $result->result ();
		
		
		/* foreach($result->result () as $res)
		{
			$res->members = $this->teamMembers ($res->team_id);
		} */
		
		return $result->result ();
		
	}
	
	
	public function teamMembers ($id)
	{
		
		$this->db->where ( 'team_id', $id );
		$this->db->select ("users.*,roles.name as rolename");
		$this->db->from ('team_member');
		$this->db->join('users','team_member.user_id = users.user_id');
		$this->db->join('roles','roles.id = users.role_id');
		$result = $this->db->get (); //echo $this->db->last_query(); die();
		return $result->result ();
		
	}

}

?>