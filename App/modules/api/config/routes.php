<?php

/*
 * Routing for Users
 */
$route['api/user'] = "User/list";
$route['api/user/add'] = "User/add";
$route['api/user/update'] = "User/update";
$route['api/user/drop/(:any)'] = "User/drop/$1";
$route['api/forgotpassword'] = "User/forgotpassword";
$route['api/changepassword'] = "User/changepassword";
$route['api/changepic'] = "User/changeprofilepic";
$route['api/user/signin'] = "User/signin";
$route['api/user/logout'] = "User/logout";
$route['api/user/verify_register'] = "User/verify_register";
$route['api/user/(:any)'] = "User/list/$1";
$route['api/getUser'] = "User/getUser";
$route['api/getcategory'] = "User/getcategory";
$route['api/getcategoryById'] = "User/getcategoryByID";
$route['api/getSubcategoryById'] = "User/getSubcategoryByID";
$route['api/getSubcategoryByCatId'] = "User/getSubcategoryByCatId";
$route['api/privacy'] = "User/privacy";
$route['api/version'] = "User/get_version";
$route['api/ask_query'] = "User/submit_query";
$route['api/getallqueries'] = "User/getAllquery";
$route['api/Aboutus'] = "User/about_us";
$route['api/Push'] = "User/push_notify";


$route['api/teamList'] = "Team/teamList";
$route['api/teamMembers'] = "Team/teamMembers";


$route['api/addgroups'] = 'Groups/addgroups';
$route['api/DeleteGroups'] = 'Groups/deletegroup';
$route['api/AddGroupMembers'] = 'Groups/add_group_members';
$route['api/RemoveGroupMember'] = 'Groups/remove_group_member';
$route['api/EditGroups'] = 'Groups/editgroups';
$route['api/addPost'] = 'Groups/add_groupPost';
$route['api/EditPost'] = 'Groups/edit_groupPost';
$route['api/DeletePost'] = 'Groups/delete_groupPost';
$route['api/getAllUsers'] = 'Groups/getAlluser';
$route['api/getAllgroups'] = 'Groups/getAllgroups';
$route['api/getGroupMembers'] = 'Groups/getgroupMembers';
$route['api/getGroupPosts'] = 'Groups/getAllgroupPosts';
$route['api/getGroupsbyUserid'] = 'Groups/getgroupByuserid';


$route['api/addTask'] = 'Tasks/addTask';
$route['api/assignTask'] = 'Tasks/assignTask';
$route['api/changeStatus'] = 'Tasks/changeStatus';
$route['api/recentTasks'] = 'Tasks/recentTasks';
$route['api/getAllTasks'] = 'Tasks/GetAllTasks';
$route['api/getTaskByTeam'] = 'Tasks/getTaskByTeam';
$route['api/getTaskByGroup'] = 'Tasks/getTaskByGroup';



$route['api/notifications'] = "Notification/notifications";
$route['api/customnotifications'] = "Notification/customnotifications";
$route['api/notifications/(:any)'] = "Notification/notifications/$1";
$route['api/usernotifications/(:any)'] = "Notification/usernotifications/$1";
?>