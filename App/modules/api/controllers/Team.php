<?php

/**
 * Created by PhpStorm.
 * User: Gaurav Ranjan<ranjan.gaurav@orangemantra.in>
 * 
 * 
 */
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
class Team extends REST_Controller {
	function __construct() {
		// Construct the parent class
		parent::__construct ();
		$this->load->model ( 'Team_model' );
		$this->load->helper ( 'string' );
	}
	
	/**
	 *
	 * @method : list Users using GET Method
	 * @method description: call to get list of Users.
	 * @param
	 *        	: care_id
	 *        	@data: Users Data
	 */
	public function teamList_get() {
		//$id = ( int ) $this->post ( 'user_id' );
		//log_message ( 'info', 'data=' . $id );
		$team = $this->Team_model->getAllTeams ();
		
		if (! empty ( $team )) {
			$this->set_response ( [ 
					'status' => true,
					'response_code' => '1',
					'message' => 'Success',
					'data' => $team 
			], REST_Controller::HTTP_OK );
		} else {
			$this->set_response ( [ 
					'status' => true,
					'response_code' => '1',
					'message' => 'No Content' 
			], REST_Controller::HTTP_OK );
		}
	}
	
	
	public function teamMembers_get() {
	
		$id = ( int ) $this->get ( 'team_id' );
		
		$team = $this->Team_model->teamMembers ($id);
	
		if (! empty ( $team )) {
			$this->set_response ( [
					'status' => true,
					'response_code' => '1',
					'message' => 'Success',
					'data' => $team
			], REST_Controller::HTTP_OK );
		} else {
			$this->set_response ( [
					'status' => FALSE,
					'response_code' => '0',
					'message' => 'No Content'
			], REST_Controller::HTTP_OK );
		}
	}
}
