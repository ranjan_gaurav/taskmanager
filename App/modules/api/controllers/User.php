<?php

/**
 * Created by PhpStorm.
 * User: Gaurav Ranjan<ranjan.gaurav@orangemantra.in>
 * 
 * 
 */
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
class User extends REST_Controller {
	function __construct() {
		// Construct the parent class
		parent::__construct ();
		$this->load->model ( 'user_model' );
		$this->load->helper ( 'string' );
	}
	
	/**
	 *
	 * @method : list Users using GET Method
	 * @method description: call to get list of Users.
	 * @param
	 *        	: care_id
	 *        	@data: Users Data
	 */
	public function list_get() {
		$id = ( int ) $this->get ( 'user_id' );
		log_message ( 'info', 'data=' . $id );
		$users = $this->user_model->getAllUsers ( $id );
		
		if (! empty ( $users )) {
			$this->set_response ( [ 
					'status' => true,
					'response_code' => '1',
					'message' => 'Success',
					'data' => $users 
			], REST_Controller::HTTP_OK );
		} else {
			$this->set_response ( [ 
					'status' => FALSE,
					'response_code' => '0',
					'message' => 'No Content' 
			], REST_Controller::HTTP_OK );
		}
	}
	public function add_post() {
		foreach ( (($this->post ())) as $key => $value ) {
			log_message ( 'info', 'data=' . $key . ' =>' . $value );
		}
		// $posted_data = $this->post();
		// $randcode = random_string('numeric', 16);
		// $random_code = substr($randcode, 0, 4);
		// $new_reg_id = $this->input->post('reg_id', true);
		// $device_id = array($new_reg_id);
		$appid = $this->input->post ( 'appid', true );
		
		// $p_id = $this->user_model->getParentID($appid);
		
		$data = array (
				'parent_id' => $this->post ( 'parentid' ),
				'app_id' => $appid,
				'email' => $this->post ( 'email' ),
				'password' => md5 ( $this->post ( 'password' ) ),
				'first_name' => $this->post ( 'first_name' ),
				'last_name' => $this->post ( 'last_name' ),
				'mobile' => $this->post ( 'mobile' ),
				'user_gender' => $this->post ( 'gender' ),
				'user_pic' => $this->post ( 'user_pic' ),
				'location' => $this->post ( 'location' ),
				'status' => 1,
				'reg_id' => $this->input->post ( 'reg_id', true ) 
		);
		
		// $create['data'] = $data;
		
		$create = $this->user_model->create ( $data );
		$this->set_response ( $create, REST_Controller::HTTP_OK );
	}
	
	/**
	 *
	 * @method : Update User using Post Method
	 * @method description: Update.
	 * @param
	 *        	: care_id, Data array
	 *        	@data: User Data
	 */
	public function update_post() {
		$id = $this->post ( 'id' );
		
		if ($id == '') {
			$message = [ 
					'response_code' => '0',
					'message' => 'Failed ! Please Send User ID.' 
			];
			$this->response ( $message, REST_Controller::HTTP_OK );
		}
		if (isset ( $id ) && $id != '') {
			
			$data = array (
					
					'email' => $this->post ( 'email' ),
					'password' => md5 ( $this->post ( 'password' ) ),
					'first_name' => $this->post ( 'first_name' ),
					'last_name' => $this->post ( 'last_name' ),
					'mobile' => $this->post ( 'mobile' ),
					'user_gender' => $this->post ( 'gender' ),
					'user_pic' => $this->post ( 'user_pic' ),
					'location' => $this->post ( 'location' ),
					'status' => 1 
			);
			
			$update = $this->user_model->update ( $id, $data );
			$user_data = $this->user_model->GetUpdateddata ( $id );
			$user_data->user_pic = $user_data->user_pic ? base_url () . 'uploads/users/profile/' . $user_data->user_pic : null;
			if ($update ['status']) {
				$status = REST_Controller::HTTP_OK;
				$update ['data'] = $user_data;
			} else {
				$status = REST_Controller::HTTP_OK;
			}
			$this->set_response ( $update, $status );
		} else {
			
			$message = [ 
					'response_code' => '0',
					'message' => 'Id Not Found' 
			];
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	
	/**
	 *
	 * @method : Delete User using DELETE Method
	 * @method description: delete User.
	 * @param
	 *        	: care__id
	 *        	@data:
	 */
	public function drop_delete() {
		$id = ( int ) $this->get ( 'user_id' );
		// Validate the id.
		if ($id <= '0') {
			$this->response ( NULL, REST_Controller::HTTP_OK );
		}
		$result = $this->user_model->delete ( $id );
		if ($result) {
			$message = [ 
					'response_code' => '1',
					'message' => 'User Deleted' 
			];
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		} else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Bad request' 
			];
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	static public function slugify($text) {
		$text = preg_replace ( '~[^\\pL\d]+~u', '_', $text );
		$text = trim ( $text, '-' );
		$text = iconv ( 'utf-8', 'us-ascii//TRANSLIT', $text );
		$text = strtolower ( $text );
		$text = preg_replace ( '~[^-\w]+~', '', $text );
		if (empty ( $text )) {
			return 'n-a';
		}
		return $text;
	}
	function signin_post() {
		$user_id = $this->post ( 'email', true );
		$password = $this->post ( 'password', true );
		$fcmRegId = $this->input->post ( 'fcmId', true );
		$deviceId = $this->input->post ( 'deviceId', true );
		$deviceType = $this->input->post ( 'deviceType', true );
		$IosID = $this->input->post ( 'reg_id_ios', true );
		
		// $device_id = json_encode(array($new_reg_id));
		$userdata = $this->user_model->validate ( $user_id, md5 ( $password ) );
		if ($userdata) {
			
			$updateDeviceData = $this->user_model->updateDeviceData ( $userdata->user_id, $deviceType, $deviceId, $fcmRegId, $IosID );
			$message = [ 
					'status' => true,
					'response_code' => '1',
					'message' => 'Success',
					'data' => $userdata 
			];
			
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}  // }
else {
			$message = [ 
					'status' => false,
					'response_code' => '0',
					'message' => 'Invalid Credentials' 
			];
			
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	public function forgotpassword_post() {
		foreach ( (($this->post ())) as $key => $value ) {
			log_message ( 'info', 'data=' . $key . ' =>' . $value );
		}
		$data = array (
				'email_id' => $this->post ( 'email_id' ),
				'mobile' => $this->post ( 'mobile' ) 
		);
		$checkCredentails = $this->user_model->CheckUserCredentials ( $data );
		if ($checkCredentails) {
			$create = $this->user_model->validate_password ( $data );
			if ($create ['status']) {
				$status = REST_Controller::HTTP_OK;
				$this->set_response ( $create, $status );
			} else {
				
				$message = [ 
						'response_code' => '0',
						'message' => $create ['message'] 
				];
				$this->set_response ( $message, REST_Controller::HTTP_OK );
			}
		} else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Sorry! Invalid Credentails.Please update Your Email and Mobile in profile or Contact Administarator!' 
			];
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	
	// change password
	function changepassword_post() {
		$user_id = $this->post ( 'user_id', true );
		$current_pass = $this->post ( 'current_pass', true );
		$new_pass = $this->post ( 'password', true );
		if ($user_id != '') {
			$userpasswrod = $this->user_model->valid_current_pass ( $user_id, md5 ( $current_pass ) );
			if (! $userpasswrod) {
				$message = [ 
						'response_code' => '0',
						'message' => 'Invalid Current Password' 
				];
				$this->set_response ( $message, REST_Controller::HTTP_OK );
			} else {
				$this->user_model->updatepassword ( 'users', $user_id, $new_pass );
				$message = [ 
						'response_code' => '1',
						'message' => 'Success',
						'new_password' => $new_pass 
				];
				$this->set_response ( $message, REST_Controller::HTTP_OK );
			}
		} else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Invalid Credentials' 
			];
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	protected function changeprofilepic_post() {
		$user_id = ( int ) $this->post ( 'care_id' );
		if (isset ( $user_id ) && $user_id != '' && $user_id != null) {
			$update = $this->user_model->updateprofile ( $user_id );
			if ($update) {
				$message = [ 
						'response_code' => '1',
						'message' => 'Success',
						'photo_id' => $update 
				];
				$this->set_response ( $message, REST_Controller::HTTP_OK );
			} else {
				$message = [ 
						'response_code' => '0',
						'message' => 'Not Updated! Please Browse File to upload' 
				];
				$this->set_response ( $message, REST_Controller::HTTP_OK );
			}
		} else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Invalid User' 
			];
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	
	// logout
	public function logout_post() {
		$user_id = $this->post ( 'care_id', true );
		$reg_id = $this->input->post ( 'android_device_id', true );
		$device = $this->input->post ( 'device_id', true );
		
		if ($user_id != '' && $reg_id != '') {
			
			// $create = $this->user_model->updaterlogout('users', $user_id, $reg_id);
			$create = $this->user_model->Deviceupdaterlogout ( 'user_devices', $user_id, $device );
			$message = [ 
					'response_code' => '1',
					'userid' => $user_id,
					'message' => 'Successfully Logout' 
			];
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		} else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Sorry! Invalid Userid. and reg id' 
			];
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	public function getUser_post() {
		$userid = $this->input->post ( 'userid' );
		
		if ($userid) {
			$Getuser = $this->user_model->GetUserInformation ( $userid );
			if ($Getuser) {
				unset ( $Getuser->password );
				
				$message = [ 
						'response_code' => '1',
						'user' => $Getuser 
				];
			} 

			else {
				$message = [ 
						'response_code' => '0',
						'message' => 'User not found' 
				];
			}
			
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	public function getcategory_post() {
		
		// $userid = $this->input->post ( 'userid' );
		$appid = $this->input->post ( 'appid' );
		
		if ($appid != '') {
			$cat = $this->user_model->fetchcategoryRecord ( $appid );
			
			if ($cat) {
				$message = [ 
						'response_code' => '1',
						'message' => 'Success',
						'data' => $cat 
				];
			} 

			else {
				$message = [ 
						'response_code' => '0',
						'message' => 'No data found',
						'data' => $subcat 
				];
			}
		} else {
			
			$message = array (
					'status' => 0,
					'response' => 'Authentication Failed' 
			);
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	public function getcategoryByID_post() {
		
		// $userid = $this->input->post ( 'userid' );
		$cat_id = $this->input->post ( 'cat_id' );
		
		if ($cat_id != '') {
			$cat = $this->user_model->fetchcategoryByID ( $cat_id );
			
			if ($cat) {
				
				$message = [ 
						'response_code' => '1',
						'message' => 'Success',
						'data' => $cat 
				];
			} 

			else {
				$message = [ 
						'response_code' => '0',
						'message' => 'No data found',
						'data' => $subcat 
				];
			}
		} else {
			
			$message = array (
					'status' => 0,
					'response' => 'Authentication Failed' 
			);
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	public function getSubcategoryByID_post() {
		
		// $userid = $this->input->post ( 'userid' );
		$subcat_id = $this->input->post ( 'subcat_id' );
		
		if ($subcat_id != '') {
			$subcat = $this->user_model->fetchsubcategoryByID ( $subcat_id );
			
			if ($subcat) {
				$message = [ 
						'response_code' => '1',
						'message' => 'Success',
						'data' => $subcat 
				];
			} 

			else {
				$message = [ 
						'response_code' => '0',
						'message' => 'No data found',
						'data' => $subcat 
				];
			}
		} else {
			
			$message = array (
					'status' => 0,
					'response' => 'Authentication Failed' 
			);
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	public function getSubcategoryByCatId_post() {
		
		// $userid = $this->input->post ( 'userid' );
		$cat_id = $this->input->post ( 'cat_id' );
		
		if ($cat_id != '') {
			$subcat = $this->user_model->fetchsubcategoryByCatID ( $cat_id );
			
			if ($subcat) {
				$message = [ 
						'response_code' => '1',
						'message' => 'Success',
						'data' => $subcat 
				];
			} 

			else {
				$message = [ 
						'response_code' => '0',
						'message' => 'No data found',
						'data' => $subcat 
				];
			}
		} else {
			
			$message = array (
					'status' => 0,
					'response' => 'Authentication Failed' 
			);
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	public function privacy_post() {
		$appID = $this->input->post ( 'appid' );
		
		if ($appID != '') {
			$privacy = $this->user_model->getPrivacyPolicy ( $appID );
			if ($privacy) {
				$message = [ 
						'response_code' => '1',
						'message' => 'Success',
						'data' => $privacy 
				];
			} 

			else {
				
				$message = [ 
						'response_code' => '0',
						'message' => 'No data found',
						'data' => $privacy 
				];
			}
		} 

		else {
			$message = array (
					'status' => 0,
					'response' => 'Authentication Failed' 
			);
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	public function get_version_post() {
		$appID = $this->input->post ( 'appid' );
		$version = $this->user_model->getversionList ( $appID );
		if (! empty ( $version )) {
			$message = array (
					'status' => true,
					'response_code' => '1',
					'message' => 'Success',
					'version_number' => $version->version_number 
			);
		} else {
			$message = array (
					'status' => FALSE,
					'response_code' => '0',
					'message' => 'No Content' 
			);
		}
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	public function submit_query_post() {
		error_reporting ( 0 );
		$data ['app_id'] = $this->input->post ( 'app_id' );
		$data ['user_id'] = $this->input->post ( 'user_id' );
		$data ['cat_id'] = $this->input->post ( 'cat_id' );
		$data ['query_desc'] = $this->input->post ( 'query' );
		$cat_name = $this->input->post ( 'cat_name' );
		
		$GetUserName = $this->db->select ( "CONCAT((first_name),(' '), last_name) AS name,mobile,email" )->where ( 'id', $data ['user_id'] )->limit ( 1 )->get ( 'users' )->row ();
		$UserQuery = $data ['query_desc'];
		
		// $mail ['username'] = $GetUserName;
		// $mail ['query'] = $data ['query_desc'];
		// echo <pre'>;print_r($mail); die();
		
		// $message = $this->load->view('basic',$mail);
		$message = '<table class="body-wrap">
	<tr>
		<td></td>
		<td class="container" bgcolor="#FFFFFF">

			<div class="content">
			<table>
				<tr>
					<td>
						<h3>Hi,</h3>
						<p class="lead"> ' . $GetUserName->name . ' has posted a new query against ' . $cat_name . ':</p>
						<p>' . $UserQuery . '</p>
										
												
						<!-- social & contact -->
						<table class="social" width="100%">
							<tr>
								<td>
									
									<!-- /column 1 -->	
									
									<!-- column 2 -->
									<table align="left" class="column">
										<tr>
											<td>				
																			
												<h5 class="">Contact Info:</h5>												
												<p>Phone: <strong>' . $GetUserName->mobile . '</strong><br/>
                Email: <strong><a href="emailto:' . $GetUserName->email . '">' . $GetUserName->email . '</a></strong></p>
                
											</td>
										</tr>
									</table><!-- /column 2 -->
									
									<span class="clear"></span>	
									
								</td>
							</tr>
						</table><!-- /social & contact -->
						
					</td>
				</tr>
			</table>
			</div><!-- /content -->
									
		</td>
		<td></td>
	</tr>
</table><!-- /BODY -->
				';
		
		if ($data ['app_id'] != '' && $data ['user_id'] != '') {
			$create = $this->user_model->ask_query ( $data );
			if ($create) {
				$user_email = $this->db->select ( 'email' )->where ( 'id', $data ['user_id'] )->limit ( 1 )->get ( 'users' )->result_array () [0] ['email'];
				$useremail = $user_email;
				$this->load->library ( 'email' );
				$config = array (
						'charset' => 'utf-8',
						'wordwrap' => TRUE,
						'mailtype' => 'html' 
				);
				// $this->email->set_mailtype ( 'html' );
				$this->email->initialize ( $config );
				$this->email->from ( $useremail );
				$this->email->to ( 'support@orangemantra.com' );
				$this->email->subject ( 'Users Query' );
				$this->email->message ( $message );
				$this->email->send ();
			}
			$this->set_response ( $create, REST_Controller::HTTP_OK );
		}
	}
	public function getAllquery_post() {
		$appid = $this->input->post ( 'app_id' );
		$userid = $this->input->post ( 'user_id' );
		
		if ($appid != '') {
			$queries = $this->user_model->GetQueriesByAppID ( $appid, $userid );
			
			if ($queries) {
				$message = [ 
						'response_code' => '1',
						'message' => 'Success',
						'data' => $queries 
				];
			} 

			else {
				$message = [ 
						'response_code' => '0',
						'message' => 'No data found' 
				];
			}
		} 

		else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Authentication Failed' 
			];
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	public function about_us_post() {
		$appid = $this->input->post ( 'app_id' );
		if ($appid != '') {
			$desc = $this->user_model->GetAppDesc ( $appid );
			// print_r($desc); die();
			if ($desc) {
				$message = [ 
						'response_code' => '1',
						'message' => 'Success',
						'about_us' => $desc->app_name 
				];
			} 

			else {
				$message = [ 
						'response_code' => '0',
						'message' => 'No data found' 
				];
			}
		} 

		else {
			$message = [ 
					'response_code' => '0',
					'message' => 'Authentication Failed' 
			];
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	public function push_notify_get() {
		$get = [ 
				"APA91bHK5uiDZnFYbz0Z20Ya1RNhKkeqS9vPhghksTyF61BuKaLzySfJi2PyMENIqF1M5aWaorl2Db15n6PGqx2tCNirGJ701fDWYMFBTTxFATiVICMTPCQ" 
		];
		
		// print_r($deviceID); die();
		
		/*
		 * $data['data'] = array(
		 * 'type' => 'Poke',
		 *
		 * );
		 */
		// print_r($get);
		
		// $notification = GetNonActionNotificationDetails($action);
		$message = 'Hi i am sending push notification';
		$api_key = 'AIzaSyAdClqgQDA9PnmXomdiUqT5JPnmQ42h-h8';
		
		if (empty ( $api_key ) || count ( $get ) < 0) {
			$result = array (
					'success' => '0',
					'message' => 'api or reg id not found' 
			);
			echo json_encode ( $result );
			die ();
		}
		$registrationIDs = $get;
		
		// $message = $msg;
		$url = 'https://android.googleapis.com/gcm/send';
		$resultData = array (
				"type" => 'type',
				"title" => 'Notification',
				"message" => $message 
		)
		;
		
		// print_r($resultData); //die();
		
		$fields = array (
				'registration_ids' => $registrationIDs,
				'data' => array (
						"message" => $message,
						"title" => 'Notification',
						"type" => 'type',
						'data' => $resultData 
				) 
		);
		// print_r($fields); die();
		$headers = array (
				'Authorization: key=' . $api_key,
				'Content-Type: application/json' 
		);
		
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
		$result = curl_exec ( $ch );
		
		curl_close ( $ch );
		print_r ( $result );
	}
}
