<?php

/**
 * Created by PhpStorm.
 * User: Gaurav Ranjan<ranjan.gaurav@orangemantra.in>
 * 
 * 
 */
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
class Tasks extends REST_Controller {
	function __construct() {
		// Construct the parent class
		parent::__construct ();
		$this->load->model ( 'Task_model' );
		$this->load->helper ( 'string' );
	}
	
	/**
	 *
	 * @method : list Users using GET Method
	 * @method description: call to get list of Users.
	 * @param
	 *        	: care_id
	 *        	@data: Users Data
	 */
	public function list_post() {
		$id = ( int ) $this->post ( 'user_id' );
		log_message ( 'info', 'data=' . $id );
		$users = $this->user_model->getAllUsers ( $id );
		
		if (! empty ( $users )) {
			$this->set_response ( [ 
					'status' => true,
					'response_code' => '1',
					'message' => 'Success',
					'data' => $users 
			], REST_Controller::HTTP_OK );
		} else {
			$this->set_response ( [ 
					'status' => FALSE,
					'response_code' => '0',
					'message' => 'No Content' 
			], REST_Controller::HTTP_OK );
		}
	}
	public function addTask_post() {
		foreach ( (($this->post ())) as $key => $value ) {
			log_message ( 'info', 'data=' . $key . ' =>' . $value );
		}
		
		// $data = $this->post();
		
		$data = array (
				'task_title' => $this->post ( 'task_title' ),
				'description' => $this->post ( 'description' ),
				'assign_type' => $this->post ( 'assign_type' ),
				'added_by' => $this->post ( 'assigned_by' ),
				'last_date' => $this->post ( 'last_date' ) ? $this->post ( 'last_date' ) : null,
				'is_assigned' => 1,
				'status' => 2 
		);
		
		// $create['data'] = $data;
		
		$create = $this->Task_model->addTask ( $data );
		$this->set_response ( $create, REST_Controller::HTTP_OK );
	}
	public function assignTask_post() {
		$data = $this->post ();
		try {
			
			// $data = $this->get();
			
			if ($data) {
				$response = $this->Task_model->assignTask ( $data );
			}
			
			$this->set_response ( $response, REST_Controller::HTTP_OK );
		} 

		catch ( Exception $e ) {
			$this->set_response ( array (
					'status' => 0,
					'message' => $e->getMessage () 
			), REST_Controller::HTTP_BAD_REQUEST );
		}
	}
	public function changeStatus_post() {
		$data ['task_id'] = $this->post ( 'task_id' );
		$data ['statusId'] = $this->post ( 'status_id' );
		$data ['note'] = $this->post ( 'note' );
		$data ['changed_by'] = $this->post ( 'user_id' );
		
		$response = $this->Task_model->changeStatus ( $data );
		
		$this->set_response ( $response, REST_Controller::HTTP_OK );
	}
	public function recentTasks_get() {
		$userid = $this->input->get ( 'user_id' );
		if ($userid != '') {
			$GetTasks = $this->Task_model->GetrecentTasks ( $userid );
			
			if ($GetTasks) {
				$message = [ 
						'status' => true,
						'response_code' => '1',
						'data' => $GetTasks 
				];
			} 

			else {
				$message = [ 
						'status' => false,
						'response_code' => '0',
						'message' => 'No Tasks Found' 
				];
			}
		} 

		else {
			$message = [ 
					'status' => false,
					'response_code' => '0',
					'message' => 'Id Not Found' 
			];
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	public function GetAllTasks_get() {
		$userid = $this->input->get ( 'user_id' );
		if ($userid != '') {
			$GetTasks = $this->Task_model->GetAllTasks ( $userid );
			
			if ($GetTasks) {
				$message = [ 
						'status' => true,
						'response_code' => '1',
						'data' => $GetTasks 
				];
			} 

			else {
				$message = [ 
						'status' => false,
						'response_code' => '0',
						'message' => 'No Tasks Found' 
				];
			}
		} 

		else {
			$message = [ 
					'status' => false,
					'response_code' => '0',
					'message' => 'Id Not Found' 
			];
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	public function getTaskByTeam_get() {
		$userid = $this->input->get ( 'user_id' );
		$teamID = $this->input->get ( 'team_id' );
		if ($userid != '') {
			$GetTasks = $this->Task_model->getTaskByTeam ( $userid, $teamID );
			
			if ($GetTasks) {
				$message = [ 
						'status' => true,
						'response_code' => '1',
						'data' => $GetTasks 
				];
			} 

			else {
				$message = [ 
						'status' => false,
						'response_code' => '0',
						'message' => 'No Tasks Found' 
				];
			}
		} 

		else {
			$message = [ 
					'status' => false,
					'response_code' => '0',
					'message' => 'Id Not Found' 
			];
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
	public function getTaskByGroup_get() {
		$userid = $this->input->get ( 'user_id' );
		
		if ($teamID != '') {
			$GetTasks = $this->Task_model->getTaskByGroup ( $userid );
			
			if ($GetTasks) {
				$message = [ 
						'status' => true,
						'response_code' => '1',
						'data' => $GetTasks 
				];
			} 

			else {
				$message = [ 
						'status' => false,
						'response_code' => '0',
						'message' => 'No Tasks Found' 
				];
			}
		} 

		else {
			$message = [ 
					'status' => false,
					'response_code' => '0',
					'message' => 'Id Not Found' 
			];
		}
		
		$this->set_response ( $message, REST_Controller::HTTP_OK );
	}
}
	

